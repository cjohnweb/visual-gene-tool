/**
 * Main API Router for HeartbeatIoT
 * @author John Minton <john.minton@jlmei.com> 
 * @version 0.0.0
 */
var Router = require('express').Router;
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var urlParser = bodyParser.urlencoded({ extended: true });

// We are going to manage API sessions here
var sessionManager = require(__dirname + '/plugins/sessionManagement'); // SQL Repository

module.exports = function (secure) {

	// make the router
	var router = Router();
	router.use(jsonParser);	// to support JSON-encoded bodies
	router.use(urlParser);	// to support URL-encoded bodies

	// Enable CORS for React to pull from API correctly...
	router.use(function (req, res, next) {
		res.header("Access-Control-Allow-Origin", "*"); // You can hit the API from anywhere
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		next(); // Pass the request along to the next route
	});

	// HTTP & HTTPS Routes for Legacy Support:
	router.post('/heartbeat.php', require(__dirname + '/routers/v1')(secure)) // LEGACY SUPPORT
	router.use('/reporting', require(__dirname + '/routers/v1')(secure)) // DEFAULT VERSION
	router.use('/v1', require(__dirname + '/routers/v1')(secure)) // LEGACY COMPATIBLE

	// Disabled Auth here for development purposes. Remove this line when finished...
	router.use('/ajax', require(__dirname + '/routers/ajax')(secure));

	// HTTPS Only Routes
	if (secure) {
		router.use('/v2', require(__dirname + '/routers/v2')(secure)) // HAS AN AUTH ROUTER IN SECURE MODE
		router.use('/v1', require(__dirname + '/routers/v1')(secure)) // LEGACY COMPATIBLE

		//All routes after this will have a session manager associated on res
		router.use(sessionPass)
		router.use('/auth', [
			require(__dirname + '/routers/auth')(secure)
		])

		router.use('/ajax', [
			authRequired,
			require(__dirname + '/routers/ajax')(secure)
		])

	} else {
		// HTTP Only Routes
		router.use('/ajax', require(__dirname + '/routers/ajax')(secure));
		// router.get('/ajax', require(__dirname + '/routers/ajax/handlers/httpsOnly'))
		//Post Request for Micro
		// router.use('/v2', require(__dirname + '/routers/v2Router')(secure)) // MINIMAL SUPPORT FOR EMBEDDED SYSTEMS
	}

	return router;

}