// Libraries and stuff
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var Promise = require('bluebird');
var sessions = require('client-sessions');
var summon = require('express-summon-route');
var url = require('url');
var yargs = require('yargs');
var jsonParser = bodyParser.json();
var urlParser = bodyParser.urlencoded({ extended: true });

// Configuration and stuff
var package = require('./package'); // Package.json file
var routers = require('./routers');
// var mySQL = require('./plugins/mysql'); // mysql connections
// var SQLRep = require(__dirname + '/plugins/sqlRep'); // SQL Repository

// ENVIRONMENT LOADS
var httpPort = process.env.HTTP_PORT || 80;
var httpsPort = process.env.HTTPS_PORT || 443;
var NODE_ENV = process.env.NODE_ENV || 'development';
var HTTPS_ENABLED = process.env.HTTPS_ENABLED || false;

// Begin the main app
var app = express(); // Initialize Express
app.disable('x-powered-by');
app.locals.Promise = Promise;
// app.locals.transformers = transformers;
// app.locals.consoleColumn = consoleColumn;
// app.locals.heartbeatDB = mySQL;
// app.locals.epoch = epoch;
// app.locals.SQLRep = SQLRep;

GLOBAL.wreck = {
  express: express,
  app: app,
};

app.locals.argv = yargs
  .help('h')
  .options({
    p: {
      alias: 'port',
      default: httpPort || 80,
      description: 'Port to run on',
    },
    h: {
      alias: 'help',
    },
  })
  .version(package.version)
  .argv;

// Routes
var bioChemTool = require('./pages/bioChemTool/routes');
var canvasTest = require('./pages/canvasTest/routes');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
summon.use(app, express);
app.use(sessions({
  cookieName: 'heartbeatIoT.sid',
  requestKey: 'session',
  secret: '$3f4#F$H$%(?<":L{P^Kf234f-92fd59);',
  duration: 7 * 24 * 60 * 60 * 1000,
  activeDuration: 1000 * 60 * 5,
  cookie: {
    path: '/',
    ephemeral: false,
    httpOnly: false,
    secure: false
  }
}));

app.use(bodyParser.json({
  extended: true,
  limit: '1024mb'
}));

// to support URL-encoded bodies
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '1024mb'
}));

app.use(bodyParser.raw({
  extended: true,
  limit: '1024mb',
  type: 'image/ *'
}))

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'))); // static asset routing, not part of login

app.use('/', bioChemTool); // Heartbeat Interface
app.use('/bioChemTool', bioChemTool); // Heartbeat Interface
app.use('/canvasTest', canvasTest); // Heartbeat Interface
app.get('/', function (req, res) {
  res.send('<a href="/bioChemTool">bioChemTool</a> <br />\n'+'<a href="/canvasTest">canvasTest</a> <br />\n');
});

app.listen(app.locals.argv.port, function () {
  console.log('NODE_ENV: ' + NODE_ENV + '\n');
  console.log('Fucking Server running on port ' + app.locals.argv.port);

});

module.exports = app;
