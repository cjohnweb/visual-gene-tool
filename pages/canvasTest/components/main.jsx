import React from 'react';
import styles from './styles';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scene: []
    };

    this.mouseClick = this.mouseClick.bind(this);
    this.getPosition = this.getPosition.bind(this);

  }

  componentDidMount() {
    let canvas = this.refs.canvas
    let ctx = canvas.getContext("2d")

    this.setState({ canvas, ctx });

    // Mouse Click Event listener
    canvas.addEventListener('click', this.mouseClick);

  }


  mouseClick(e) {
    let pos = this.getPosition(e);
    let x = e.pageX;
    let y = e.pageY;
    let ctx = this.state.ctx;
    ctx.setLineDash([]);
    ctx.beginPath();
    ctx.ellipse(x, y, 40, 20, 0, 0, 2 * Math.PI);
    ctx.stroke();
  }

  getPosition(element) {
    // I have no fucking idea why this is necessary but...
    // occasionally getBoundingClientRect is undefined
    if (!element || !element.getBoundingClientRect) {
      return [0, 0]
    }
    // client rect is more accurate. Firefox didn't like the above method.
    var rect = element.getBoundingClientRect();
    return [rect.left, rect.top];
  }

  render() {
    return (<div {...styles.page.container}>
      <div {...styles.page.header}>
        <h1 style={{ align: "center" }}>BioChem Tool</h1><br />
      </div>
      <div {...styles.page.main}>
        <canvas ref="canvas" style={{ border: "1px solid #000000" }} width={640} height={425} />
      </div>
    </div>);
  }
}

export default Main;