import React from 'react';

import Canvas from './canvas'; // Main component that is holding all the scene data
import SceneMenu from './sceneMenu'; // Main component that is holding all the scene data

class SceneController extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tool: 'plotNewGene',
      selectedGeneID: null,
      scene: [], // Holds everything in the scene to be rendered
      canvas: null,
      canvasContext = null
    };

    // bind functinos

  }

  ComponentWillMount() {
    // Event Listeners
    this.state.canvas.addEventListener('click', this.mouseClick);
    this.state.canvas.addEventListener('mousedown', this.mouseDown);
    this.state.canvas.addEventListener('mouseup', this.mouseUp);
    this.state.canvas.addEventListener('resize', this.onResize);
    document.addEventListener('keypress', function (e) { console.log("PRESSED: ", e); });
  }

  plotGene = function (x, y) {
    let scene = this.state.scene;
    scene.push({
      name: "Gene",
      timestamp: Date.now(),
      x: x,
      y: y,
      gene: {
        rsid: '0',
      },
      geometry: {
        shape: 'elipse',
        radiusX: 40,
        radiusY: 25
      },
      userData: {}
    });
    scene[scene.length - 1].id = scene.length - 1; // Load the gene with an ID (same as the array key)
    this.setState({ scene });
  }

  setSelectedGene = function (geneID) {
    selectedGeneID = geneID;
  }

  clearSelectedGene = function () {
    selectedGeneID = null;
  }

  getSelectedGene = function () {
    return selectedGeneID;
  }

  captureKeypress = function (e) {
    console.log("EVENT: ", e);
    if (e.keyCode === 27) {
      console.log("Esc Pressed");
      clearSelectedGene();
    }
  }

  mouseClick = function (e) {
    let pos = getPosition(this);
    let mouseX = e.pageX;
    let mouseY = e.pageY;
    let checkClickForHit = null;

    if (tool == "selector") {
      // console.log("Determining if the click (" + mouseX + 'x' + mouseY + ") was within the space of a gene.");
      loopScene(function (gene, i) {
        let rx = gene.geometry.radiusX;
        let ry = gene.geometry.radiusY;
        if ((mouseX >= (gene.x - rx) && mouseX <= (gene.x + rx))
          && (mouseY >= (gene.y - ry) && mouseY <= (gene.y + ry))) {
          console.log("Mouse Click Selector Tool: ", gene)
          checkClickForHit = gene;
        }
      });
      if (checkClickForHit && checkClickForHit.id > 0) {
        // If there is at least one gene present, select the last entry in this array. (Only select 1 at a time)
        setSelectedGene(checkClickForHit.id);
      } else {
        clearSelectedGene();
      }
    }

    if (tool == "plotNewGene") {
      // console.log("Determining if it is ok to plot a gene at click " + mouseX + 'x' + mouseY + ".");
      loopScene(function (gene, i) {

        if (mouseX >= gene.x - (gene.geometry.radiusX * 2) && mouseX <= gene.x + (gene.geometry.radiusX * 2)
          && mouseY >= gene.y - (gene.geometry.radiusY * 2) && mouseY <= gene.y + (gene.geometry.radiusY * 2)) {
          console.log("Mouse Click plotNewGene Tool: ", gene)
          checkClickForHit = gene;
        }
      });
      if (checkClickForHit && checkClickForHit.id) {
        setSelectedGene(checkClickForHit.id);
      } else {
        // plot a new Gene
        plotGene(mouseX, mouseY);
      }

    }
  }

  populateGeneMenu = function () {
    let theActions = document.getElementById('theActions');
    let res;
    loopScene(function (gene, i) {
      let span = gene.selected === true ? '<span style="background-color: #4caf50;">' : '<span>';
      res = res ? res + span + gene.name + '</span><br />' : span + gene.name + '</span><br />';
    });

    if (res) {
      theActions.innerHTML = res;
    }
  }

  drawGene = function (gene) {
    ctx.fillStyle = 'rgb(200, 200, 200)';
    // console.log("ctx: ", ctx);
    // Draw the Elipse
    ctx.setLineDash([]);
    ctx.beginPath();
    ctx.ellipse(gene.x, gene.y, gene.geometry.radiusX, gene.geometry.radiusY, 0, 0, 2 * Math.PI);
    ctx.stroke();

    // Draw the Gene Name / Text
    let txt = gene.name;
    ctx.font = "14px Arial";
    ctx.fillStyle = '#000';
    ctx.fillText(txt, gene.x - (ctx.measureText(txt).width / 2), gene.y + 5);

    // console.log(" getSelectedGene(): ", getSelectedGene());

    let selected = getSelectedGene();
    // If selected, draw the selections

    if (selected == null) {
      // console.log("no gene selected");
      $('#menu').css('display', 'none');
    } else if (selected == gene.id) {
      // console.log("Gene selected: ", selected);
      // console.log("Gene ID: ", gene.id);
      // If this gene is the selected gene...draw the selection
      let rx = gene.geometry.radiusX; // * 1.2;
      let ry = gene.geometry.radiusY; // * 1.2;
      ctx.rect(gene.x - rx, gene.y - ry, rx * 2, ry * 2);
      ctx.stroke();
      // If selected also show menu...
      $('#menu')
        .css('display', 'block')
        .css('top', gene.y)
        .css('left', gene.x);
    }

    // ctx.setLineDash([5, 5])
    // ctx.moveTo(0, 200);
    // ctx.lineTo(200, 0);
    // ctx.stroke();

  }


  loopScene = function (cb) {
    if (typeof cb === "function") {
      scene.forEach(function (gene, i) {
        cb(gene, i);
      });
    } else {
      return false;
    }
  }

  createEvent = function (name, detail) {
    let defaultDetail = {
      time: new Date()
    };
    detail = Object.assign({}, defaultDetail, detail);
    var event = new CustomEvent(
      name,
      { detail }
    );
    obj.dispatchEvent(event);
  }


  setActiveSelectionByXY = function (x, y) {
    // loop through scene and determine if we anything was clicked based on the x, y and geometery.radius values.
    // return geneID
    // Loop thrrough geneID's and mark as selected = true;
    //scene[geneID].selected = true;
  }

  setActiveSelectionByID = function (geneID) {
    // Just mark it as selected
    scene[geneID].selected = true;
  }

  selectTool = function (selection) {
    if (selection == 'selector' || selection == 'plotNewGene') {
      // console.log("Selector Tool: ", selection);
      tool = selection;
    }

    if (selection == 'selector') {
      $('#selectSelectorTool').css('background-color', '#ffbbbb');
      $('#selectPlotNewGeneTool').css('background-color', '#ffffff');
    }

    if (selection == 'plotNewGene') {
      $('#selectSelectorTool').css('background-color', '#ffffff');
      $('#selectPlotNewGeneTool').css('background-color', '#ffbbbb');
    }

  }

  unselectAll = function () {
    scene.forEach(function (gene) {
      gene.selected = false;
    });
  }

  getPosition = function (element) {
    // I have no fucking idea why this is necessary but...
    // occasionally getBoundingClientRect is undefined
    if (!element || !element.getBoundingClientRect) {
      return [0, 0]
    }
    // client rect is more accurate. Firefox didn't like the above method.
    var rect = element.getBoundingClientRect();
    return [rect.left, rect.top];
  }


  // Render then builds...
  // Object to send to canvas
  // Object to send to menu
  // 


  render() {

    let scene = {
      display: {}, // Instructions to canvas
      menu: {}, // Instructions to menu?
    };

    return (<div>
      <div style={{ width: "1000px", border: "1px solid #000000" }}>
        <div style={{ width: "200px", height: "200px" }}>
          <SceneMenu scene={scene} />
        </div>
        <br /><br />
        <div style={{ width: "800px" }}>
          <Canvas scene={scene} />
        </div>
      </div >
    </div>);
  }
}

export default SceneController;
