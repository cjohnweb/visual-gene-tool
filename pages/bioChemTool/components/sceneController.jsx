import React from 'react';
import { style, nthChild } from 'glamor';
import styles from './styles';

class SceneController extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scene: [], // Holds everything in the scene to be rendered
      scenePropList: [],
      tool: 'plotNewGene',
      mouseDown: false,
      propInspector: null,
      selection: null,
      dragging: [],
      canvasContainer: null,
      canvas: null,
      canvasHeight: 600,
      canvasWidth: 900,
      ctx: null,
      boundaryMultiplier: 2,
      offset: 200
    };

    this.mouseClick = this.mouseClick.bind(this);
    this.mouseMove = this.mouseMove.bind(this);
    this.mouseDown = this.mouseDown.bind(this);
    // this.mouseUp = this.mouseUp.bind(this);
    this.keyPress = this.keyPress.bind(this);
    this.getPosition = this.getPosition.bind(this);
    this.renderScene = this.renderScene.bind(this);
    this.renderGene = this.renderGene.bind(this);
    this.renderRelation = this.renderRelation.bind(this);
    this.loopObject = this.loopObject.bind(this);
    this.detectAtCoordinates = this.detectAtCoordinates.bind(this);
    this.updateDimensions = this.updateDimensions.bind(this);
    this.setSelection = this.setSelection.bind(this);
    this.renameProp = this.renameProp.bind(this);
  }

  componentDidMount() {
    // Get canvas reference and 2d context into state
    let canvas = this.refs.canvas;
    let ctx = canvas.getContext("2d");
    let canvasContainer = this.refs.canvasContainer;

    this.setState({ canvasContainer, canvas, ctx });

    // Update canvas dimensions
    this.updateDimensions();

    // resize event listener, for automatically resizing the canvas and page layout
    window.addEventListener("resize", this.updateDimensions);

    // Keyboard and Mouse Event listeners
    canvas.addEventListener('mousemove', this.mouseMove);
    canvas.addEventListener('click', this.mouseClick); // Same as mouseUp
    canvas.addEventListener("mousedown", this.mouseDown);
    document.addEventListener('keydown', this.keyPress);

  }

  componentWillUpdate(nextProps, nextState) {


    if (this.state.scene !== nextState.scene) {
      this.buildMenu();
    }
    // if (this.state.scene !== nextState.scene) {
    //   console.log("Scene Changed");
    // }
  }

  updateDimensions() {

    /** To Do
     * I need to research this more and figure out exactly how I want to do this. 
     * For the time being, Hard code values to look good on my screen.
     * */

    // let canvas = this.state.canvas;
    // let canvasContainer = this.state.canvasContainer;
    // let offset = this.state.offset;

    // // // Adjust the canvas size based on the size of the canvasContainer
    // if (canvasContainer && canvasContainer.clientHeight && canvasContainer.clientWidth) {

    //   let totalWidth = window.innerWidth;
    //   let totalHeight = window.innerHeight;
    //   let canvasWidth = window.innerWidth - offset;
    //   let canvasHeight = window.innerHeight - offset;

    //   //   console.log("canvasContainer.clientHeight - offset: ", canvasContainer.clientHeight - offset);
    //   //   console.log("canvasContainer.clientWidth - offset: ", canvasContainer.clientWidth - offset);

    //   // console.log("window.innerWidth: ", window.innerWidth);
    //   // console.log("window.innerHeight: ", window.innerHeight);

    //   // console.log("canvasContainer.clientHeight: ", canvasContainer.clientHeight);
    //   // console.log("canvasContainer.clientWidth: ", canvasContainer.clientWidth);

    //   //   console.log("canvas.clientHeight: ", canvas.clientHeight);
    //   //   console.log("canvas.clientWidth: ", canvas.clientWidth);

    //   if ((this.state.canvasHeight != canvasContainer.clientHeight - offset) ||
    //     (this.state.canvasWidth != canvasContainer.clientWidth - offset)) {
    //     // console.log("updateDimensions canvasContainer: ", canvasContainer, canvasContainer.clientHeight, canvasContainer.clientWidth);
    //     this.setState({ canvasHeight: canvasHeight, canvasWidth: canvasWidth });
    //   }
    // }
  }

  loopObject(type, cb) {
    if (typeof cb == "function") {
      let scene = this.state.scene;
      let genes = scene
        .filter((prop) => {
          return prop && prop.type == type;
        })
        .forEach((gene) => {
          cb(gene);
        });
    }
  }

  detectAtCoordinates(x, y) {
    // Grab the scene, selection, etc
    let scene = this.state.scene;
    let boundaryMultip = this.state.boundaryMultiplier;
    let detectedObjects = []; // if a click hits something in the scene, this holds it's ID

    // Genes
    this.loopObject("gene", (gene, i) => {
      let rx = gene.geometry.radiusX * boundaryMultip;
      let ry = gene.geometry.radiusY * boundaryMultip;

      let xleftB = gene.x - rx;
      let xrightB = gene.x + rx;
      let yleftB = gene.y - ry;
      let yrightB = gene.y + ry;

      if ((x >= (gene.x - rx) && x <= (gene.x + rx))
        && (y >= (gene.y - ry) && y <= (gene.y + ry))) {
        detectedObjects.push(gene.id);
      }
    });

    return detectedObjects;

  }

  mouseDown(e) {

    // console.log("MouseDown: ", e);
    this.setState({ mouseDown: true });

    // Get the canvas and context
    let canvas = this.state.canvas;
    let ctx = this.state.ctx;

    // Get the position of the click's x & y coordinates
    let pos = this.getPosition(e);
    let x = e.pageX;
    let y = e.pageY;

    // Apply offsets to the x & y coordinates as at this point in development we need these.
    x = x - e.pageX + e.offsetX;
    y = y - e.pageY + e.offsetY;

    let dragging = this.detectAtCoordinates(x, y);
    this.setState({ dragging });

  }

  mouseMove(e) {
    // Adjust canvas size
    //this.updateDimensions();

    // Get the canvas and context
    let canvas = this.state.canvas;
    let ctx = this.state.ctx;
    let scene = this.state.scene;
    let boundaryMultip = this.state.boundaryMultiplier
    let mouseDown = this.state.mouseDown;

    // Get the position of the click's x & y coordinates
    let pos = this.getPosition(e);
    let x = e.pageX;
    let y = e.pageY;

    // Apply offsets to the x & y coordinates as at this point in development we need these.
    x = x - e.pageX + e.offsetX;
    y = y - e.pageY + e.offsetY;

    let dragging = this.state.dragging;

    if (dragging.length > 0) {
      if (true) { // check to make sure not draggin over other object in scene here


        /**
         * Simply do the same check that I do for collisons.
         * 
         * If collison, highlight in RED while dragging
         * 
         * If let go from dragging while in this red state, compare where the mouse is and where the 
         * closest edge is on x and y axis and drop it over there so it's far enough away.
         */

        //move the gene to these x y
        // console.log("Dragging0: ", dragging[0], scene[dragging[0]]);
        scene[dragging[0]].x = x;
        scene[dragging[0]].y = y;
      }
    }

    // Highlight items we mouse over
    // Genes
    this.loopObject("gene", (gene, i) => {
      let rx = gene.geometry.radiusX * boundaryMultip;
      let ry = gene.geometry.radiusY * boundaryMultip; // the * 2 will remain constant, but will change when I introduce zoom features

      let xleftB = gene.x - rx;
      let xrightB = gene.x + rx;
      let yleftB = gene.y - ry;
      let yrightB = gene.y + ry;

      if ((x >= (gene.x - rx) && x <= (gene.x + rx))
        && (y >= (gene.y - ry) && y <= (gene.y + ry))) {
        scene[gene.id].highlight = true;
      } else if (scene[gene.id].highlight != false) {
        scene[gene.id].highlight = false;
      }
    });

    this.setState({ scene });

  }

  mouseClick(e) { // mouseClick && mouseUp
    // console.log("mouseClick/Up`");
    this.setState({ mouseDown: false, dragging: [] }); // might consider adding 50 ms delay here

    // Get the canvas and context
    let canvas = this.state.canvas;
    let ctx = this.state.ctx;
    let scene = this.state.scene;
    let selection = this.state.selection;

    // Get the position of the click's x & y coordinates
    let pos = this.getPosition(e);
    let x = e.pageX;
    let y = e.pageY;

    // Apply offsets to the x & y coordinates as at this point in development we need these.
    x = x - e.pageX + e.offsetX;
    y = y - e.pageY + e.offsetY;

    let newSelection = this.detectAtCoordinates(x, y);
    newSelection = newSelection[0];

    if (newSelection || newSelection == 0) {

      selection = newSelection;

    } else {

      let gene = {
        id: null,
        type: "gene",
        name: "Gene Name",
        timestamp: Date.now(),
        x: x,
        y: y,
        gene: {
          rsid: '0',
        },
        geometry: {
          shape: 'elipse',
          radiusX: 40,
          radiusY: 20
        },
        userData: {}
      };

      scene.push(gene);
      selection = scene.length - 1;
      scene[selection].id = selection; // Load the gene with an ID (same as the array key)

    }

    this.setState({ scene, selection });

  }

  keyPress(e) {

    let scene = this.state.scene;
    let selection = this.state.selection;

    // console.log("Keypress: ", e);

    /** Example, values from up arrow key
      altKey: false
      charCode: 0
      code: "ArrowUp"
      ctrlKey: false
      key: "ArrowUp"
      keyCode: 38
      shiftKey: false
      timeStamp: 75771.89999999246
     */

    // Arrow Keys - Move the object

    let arrowKeys = [
      {
        key: "up",
        code: "ArrowUp",
        keyCode: 38,
        axis: "y",
        increment: -1
      },
      {
        key: "down",
        code: "ArrowDown",
        keyCode: 40,
        axis: "y",
        increment: 1
      },
      {
        key: "left",
        code: "ArrowLeft",
        keyCode: 37,
        axis: "x",
        increment: -1
      },
      {
        key: "right",
        code: "ArrowRight",
        keyCode: 39,
        axis: "x",
        increment: 1
      }
    ];

    if (selection == 0 || selection) {
      arrowKeys.forEach((arrow) => {
        if (arrow.keyCode == e.keyCode) {
          // scene [selection].[x or y] += 1 or -1 ie arrow keys will move the xy coordinates
          if (e.shiftKey) {
            // console.log("YES SHIFT KEY");
            scene[selection][arrow.axis] = scene[selection][arrow.axis] + (arrow.increment * 20);
          } else {
            // console.log("YES BUT NO SHIFT KEY");
            scene[selection][arrow.axis] = scene[selection][arrow.axis] + arrow.increment;
          }
        }
      });
    }

    // Delete the selection, if any, with delete key
    if ((selection == 0 || selection) && (e.key == "Delete" || e.keyCode == 46)) {
      scene[selection] = null;
    }

    // Escape key un-selects object in scene
    if (e.keyCode == 27) {
      selection = null;
    }

    this.setState({ scene, selection });

  }

  getPosition(element) {
    // I have no fucking idea why this is necessary but...
    // occasionally getBoundingClientRect is undefined
    if (!element || !element.getBoundingClientRect) {
      return [0, 0]
    }
    // client rect is more accurate. Firefox didn't like the above method.
    var rect = element.getBoundingClientRect();

    return [rect.left, rect.top];
  }


  renderGene(gene) {
    let x = gene.x;
    let y = gene.y;
    let ctx = this.state.ctx;
    let boundaryMultip = this.state.boundaryMultiplier
    let geneColor = "#000000";

    // Change color based on highlight true / false
    if (gene.highlight) {
      geneColor = "#ff0000";
    }

    // Set draw color
    ctx.fillStyle = geneColor;
    ctx.strokeStyle = geneColor;

    // Draw Name
    ctx.font = "14px Courier New";
    ctx.fillText(gene.name, gene.x - (ctx.measureText(gene.name).width / 2), gene.y + 5);

    let newRadiusX = ctx.measureText(gene.name).width;

    console.log("newRadiusX: ", newRadiusX);

    // Draw Gene
    ctx.setLineDash([]);
    ctx.beginPath();
    ctx.ellipse(x, y, 32, gene.geometry.radiusY, 0, 0, 2 * Math.PI);
    ctx.stroke();

    // Mark Gene as selection
    if (this.state.selection == gene.id) {
      let rx = gene.geometry.radiusX * boundaryMultip;
      let ry = gene.geometry.radiusY * boundaryMultip;
      ctx.rect(gene.x - rx, gene.y - ry, rx * boundaryMultip, ry * boundaryMultip);
      ctx.stroke();
    }

    if (gene.highlight) {
      ctx.strokeStyle = "#ffff00";
      let rx = gene.geometry.radiusX * boundaryMultip;
      let ry = gene.geometry.radiusY * boundaryMultip;
      ctx.rect(gene.x - rx, gene.y - ry, rx * boundaryMultip, ry * boundaryMultip);
      ctx.stroke();
    }

  }

  renderRelation(relation) {
    let x = relation.x;
    let y = relation.y;
    let ctx = this.state.ctx;

    ctx.font = "14px Arial";
    ctx.fillStyle = '#000';
    ctx.fillText(relation.name, relation.x - (ctx.measureText(relation.name).width / 2), relation.y + 5);

    ctx.setLineDash([]);
    ctx.beginPath();
    ctx.ellipse(x, y, relation.geometry.radiusX, relation.geometry.radiusY, 0, 0, 2 * Math.PI);
    ctx.stroke();

  };

  renderScene() {
    let canvas = this.state.canvas;
    let ctx = this.state.ctx;

    if (canvas && ctx) {

      // Clear the canvas
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      // Loop through and write all the objects to canvas
      let scene = this.state.scene;
      // console.log("Count scene: ", scene.length);

      scene.filter((prop) => { return prop && prop.type == "gene"; }).forEach((gene) => {
        // console.log("Gene: ", gene);
        this.renderGene(gene);
      });

    }
  }

  setSelection(e) {
    console.log("setSelection: typeof e: ", typeof e);
    if (typeof e == "object") {
      this.setState({ selection: e.target.dataset.geneID });
    }
  }

  renameProp(e) {
    let id = e.target.dataset.propid;
    let scene = this.state.scene;
    let prop = scene[id] ? scene[id] : null;
    if (prop) {
      prop.name = e.target.value;
      scene[id] = prop;
    }
    this.setState({ scene });
  }

  render() {

    // redraw the canvas
    this.renderScene();

    // Bring in everything we need...
    let canvas = this.state.canvas;
    let canvasContainer = this.state.canvasContainer;
    let canvasHeight = this.state.canvasHeight;
    let canvasWidth = this.state.canvasWidth;
    let selection = this.state.selection;
    let scene = this.state.scene;

    // Build the Scene's Prop List
    let scenePropList = scene
      .filter((prop) => {
        return prop && prop.type == "gene";
      })
      .map((gene, i) => {
        let theStyle = style({ backgroundColor: '#ffffff' });
        if (this.state.selection == gene.id) {
          theStyle = style({ backgroundColor: '#ff0000' });
        }
        return (<li {...theStyle} key={i} data-geneID={gene.id} onClick={this.setSelection}> {gene.name}</li >);
      });

    // Build Inspector Menu
    let propInspector = null;
    if (selection == 0 || !isNaN(selection)) {
      if (selection == 0 || selection) {
        let iProp = this.state.scene[selection];
        propInspector = (<div>
          <div>Object Properties</div>
          <ul>
            <li>Type: {iProp.type}</li>
            <li>XY: {iProp.x} x {iProp.y}</li>
          </ul>
          <div>Name: <input data-propid={iProp.id} onChange={this.renameProp} value={iProp.name} /></div>
        </div>);
      }
    }


    // Build the menu out. Make the hover menu that shows up on selection. 
    // Worry about moveing the menu to another file later, just get it in here now.
    // create a trash can that can drag a gene to the "trash" and delete it.
    // Create some kind of toolbar menu to allow the user to select a tool in HTML in the menu on the side.
    // Make Gene size varry depending on size of text in gene
    // Prevent dragging a gene onto another gene

    //   <div {...styles.page.container}>
    //   <div {...styles.page.header}>
    //     <h1 style={{ align: "center" }}>BioChem Tool</h1><br />
    //   </div>
    // <div {...styles.page.main}>


    return (<div style={{
      display: "grid",
      gridTemplateColumns: "900px auto",
      gridTemplateRows: "600px",
      margin: "0px",
      padding: "0px",
      border: "1px solid #ff0000"
    }}>
      <div
        ref="canvasContainer"
        style={{
          gridColumn: "1 / span 1",
          gridColumnEnd: "2 / span 1",
          border: "0px solid #000000",
          margin: "0px",
          padding: "0px"
        }}>
        <canvas ref="canvas" style={{ border: "1px solid #000000" }} width={canvasWidth} height={canvasHeight} />
      </div>
      <div style={{
        gridColumn: "2 / span 1",
        gridRow: "1 / span 1",
        border: "1px solid #000000",
        margin: "0px",
        padding: "0px"
      }}>
        <div style={{
          display: "grid",
          gridTemplateColumns: "230px 230px",
          gridTemplateRows: "600px",
          margin: "0px",
          padding: "0px"
        }}>

          <div ref="sceneList" style={{
            gridColumn: "1 / span 1",
            gridRow: "1 / span 1",
            border: "1px solid #000000",
            margin: "0px",
            padding: "10px"
          }}>
            {scenePropList}
          </div>
          <div ref="menu" style={{
            gridColumn: "2 / span 1",
            gridRow: "1 / span 1",
            border: "1px solid #000000",
            margin: "0px",
            padding: "10px"
          }}>
            {propInspector}
          </div>

        </div>
      </div>
    </div>);

    {/* </div>
      </div> */}

  }
}

export default SceneController;