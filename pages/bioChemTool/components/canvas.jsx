import React from 'react';
import styles from './styles';

class Canvas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };

    this.updateCanvas = this.updateCanvas.bind(this);
  }

  componentDidMount() {
    this.updateCanvas();
  }

  componentDidUpdate() {
    this.updateCanvas();
  }

  updateCanvas() {
    // This is probably where I would take in all the scene data and plot it out to canvas elements...
    const canvas = this.refs.canvas;
    const canvasContainer = this.refs.canvasContainer;

    const ctx = canvas.getContext('2d');

    // Update canvas size
    let width = parseInt(window.getComputedStyle(canvasContainer, null).getPropertyValue("width"));
    let height = parseInt(window.getComputedStyle(canvasContainer, null).getPropertyValue("height"));
    if (canvas.width != width || canvas.height != height) {
      canvas.width = width;
      canvas.height = height;
    }

    ctx.clearRect(0, 0, 300, 300);

    rect({ ctx, x: 10, y: 10, width: 50, height: 50 });
    rect({ ctx, x: 110, y: 110, width: 50, height: 50 });
  }

  componentWillReceiveProps(nextProps) {

  }

  animate() {

    // clear canvas & draw the Scene
    ctx.clearRect(0, 0, _this.canvas.width, _this.canvas.height);

    loopScene(function (gene, i) {
      // console.log("Loopscene Gene: ", gene);
      drawGene(gene);
    });

    // // Now determine a kind of "state" to write our HTML overlays and such
    populateGeneMenu(); // Load the list of Genes on the right toolbar

    // // Resize the canvas if the canvas has changed size
    updateCanvasSize();

    // call again next time we can draw. Does this go at the beginning or end of the loop??
    requestAnimationFrame(animate);
  }

  render() {
    // These need to be set here and passed to sceneController I think...
    let canvasContainer = document.getElementById('canvasContainer');
    let canvas = document.getElementById('canvas');




    let geneContextMenu = (<div id={'menu'} style={{
      position: 'absolute',
      display: 'none',
      top: '0',
      left: '0',
      height: '250px',
      width: '200px',
      zIndex: '99',
      border: '1px solid #000000',
      backgroundColor: '#efefef',
      borderRadius: '5px'
    }} >
      <input name={'name'} onChange={() => { updateGene(gene); }} />
      <div id='options'>
        <ul>
          <li>xxx</li>
        </ul>
      </div>
    </div>);

    return (<div ref="canvasContainer" id={'canvasContainer'}>
      <canvas ref="canvas" id="canvas" style={{ position: 'absolute', border: '1px solid #000000', zIndex: '50' }} width={"800"} height={"636"}>
        Your browser doesn't support Canvas. Please update your browser or install Google Chrome for the best experience on the web.
      </canvas>
      {geneContextMenu}
    </div>);
  }
}

export default Canvas;