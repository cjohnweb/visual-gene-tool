import React from 'react';
import styles from './styles';

// Primary Components

class SceneMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {

    return (<div>

      <ul>
        <li id="selectSelectorTool">
          <a href={"#"} onClick={() => { selectTool('selector'); }}>Selector Tool</a>
        </li>
        <li id="selectPlotNewGeneTool">
          <a href={"#"} onClick={() => { selectTool('plotNewGene'); }}>Place Gene</a>
        </li>
      </ul>

      <br />

      <b>Scene Menu</b>
      <div id={'theActions'} style={{ height: '400px', overflow: 'scroll' }}>

      </div>
    </div>);
  }
}

export default SceneMenu;