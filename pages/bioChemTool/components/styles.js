import { style, nthChild } from 'glamor';

const styles = {
    navigation: {
        container: style({
            borderBottom: "1px solid #bcbcbc",
            display: "flex",
            flexDirection: "row",
            flexWrap: "nowrap",
            height: "40px",
            justifyContent: "flex-start",
            width: "100%"
        }),
        tab: style({
            border: "1px solid #bcbcbc",
            borderRadius: "10px",
            width: "150px",
            height: "20px",
            marginLeft: "20px",
            textAlign: "center",
            paddingTop: "2px",
            cursor: "pointer",
            backgroundColor: "#ffffff",
            transition: "background-color 0.25s ease",
            ":hover": {
                backgroundColor: "#feff29"
            }
        }),
        selected: style({
            transition: "background-color 0.25s ease",
            backgroundColor: "#9cc0ff"
        })
    },
    page: {
        container: style({
            border: "0px solid #000000",
            position: "fixed",
            top: "0",
            left: "0",
            padding: "0px",
            margin: "0px",
            width: "100%",
            height: "100%",
            fontFamily: "'Futura Light', Arial",
            color: "#000000",
            size: "1rem"
        }),
        header: style({
            border: "0px solid #000000",
            display: "block",
            width: "100%",
            height: "75px",
            margin: "10px auto",
            textAlign: "center"
        }),
        main: style({
            border: "0px solid #000000",
            display: "block",
            height: "Calc(100% - 80px)",
            margin: "10px auto",
            marginTop: "40px",
            minWidth: "650px",
            overflow: "auto",
            width: "80%"
        }),
        formBody: style({
            border: "0px solid #000000",
            display: "block",
            margin: "10px auto"
        })
    },
    buttons: {
        buttonsContainer: style({
            borderTop: "1px solid #bcbcbc",
            display: "flex",
            flexDirection: "row",
            flexWrap: "nowrap",
            height: "40px",
            padding: "10px",
            justifyContent: "flex-start",
            width: "100%"
        }),
        buttonContainer: style({
            border: "1px solid #bcbcbc",
            borderRadius: "10px",
            width: "150px",
            height: "20px",
            marginLeft: "6px",
            textAlign: "center",
            paddingTop: "2px",
            cursor: "pointer",
            backgroundColor: "#efefef",
            transition: "background-color 0.25s ease",
            ":hover": {
                backgroundColor: "#9cc0ff"
            }
        }),
    },
    formBuilder: {
        container: style({
            display: "block",
            width: "100%",
            margin: "0px",
            padding: "0px"
        }),
        entry: style({
            display: "block",
            width: "90%",
            minHeight: "50px",
            border: "1px solid #000000",
            margin: "10px",
            padding: "20px"
        }),
        subEntry: style({
            border: "1px solid #000000",
            backgroundColor: "#cbcbff",
            display: "block",
            minHeight: "75px",
            margin: "10px",
            marginLeft: "20px",
            padding: "20px",
            width: "calc(90% - 10px)",
        })
    },

    form: {
        label: style({
            display: "flex",
            flexDirection: "right",
            fontFamily: "'Futura Light'",
            fontSize: "1.1rem",
            margin: "0px 0px 10px 0px",
            padding: "0px",

        }),
        text: style({
            color: "#adadad",
            fontSize: ".9rem",
            lineHeight: "1.5",
            width: "100px",
            display: "block",
        }),
        value: style({
            marginTop: "0px",
            display: "block",
            border: "0",
            outline: "0",
            borderBottom: "1px solid #4B4B4B",
            fontFamily: "'Futura Light'",
            fontSize: "1rem",
            width: "200px",
            // marginLeft: "20px"
        }),
    },

    container: style({
        height: "100%",
        width: "100%",
        marginBottom: "40px"
    }),
    pageContainer: style({
        width: "1024px",
        margin: "0 auto",
    }),
    main: style({
        width: "1024px",
        margin: "0 auto",
    }),
    section: style({
        width: "100%"
    }),
    hidden: style({
        display: "none"
    }),
    searchFilterInputContainer: style({
        display: "flex",
        justifyContent: "row",
        textAlign: "left",
        width: "1024px",
        margin: "0px auto",
        paddingTop: "11px",
        paddingBottom: "14px",
        border: "0px",
        lineHeight: "1.3rem"
    }),
    searchFilterLabel: style({
        width: "200px",
        lineHeight: "1.7rem",
        fontSize: "1.2rem"
    }),
    searchFilter: style({
        paddingLeft: "10px",
        width: "400px",
        lineHeight: "1.7rem",
        fontSize: "1.2rem",
        border: "1px solid #000000",

    }),
    widgetListItem: style({
        width: "100%"
    }),
    deviceListTable: style({
        width: "1024px",
        border: "0px solid #000000"
    }),
    deviceContainer: style({
        border: "1px solid #000000"
    }),
    clearBoth: style({
        width: "100%",
        display: "block",
        clear: "both"
    }),
    highlighter: style({
        backgroundColor: "#ff0000"
    }),
    clearTextLabel: style({
        color: "#adadad",
        fontSize: ".9rem",
        lineHeight: "1.5",
        width: "100px",
        display: "block",
    }),
    clearTextValue: style({
        color: "#000000",
        fontSize: ".9rem",
        lineHeight: "1.5",
        width: "250px",
        display: "block",
    }),
    clearText: style({
        lineHeight: "1.5",
        fontSize: "1rem"
    }),
    altTableTRs: style(
        nthChild('even', {
            backgroundColor: "#ffffff",
            cursor: "pointer",
            transition: "background-color 0.25s ease",
            ":hover": {
                backgroundColor: "#0096ff"
            }
        }),
        nthChild('odd', {
            backgroundColor: "#f6f6f6",
            cursor: "pointer",
            transition: "background-color 0.25s ease",
            ":hover": {
                backgroundColor: "#0096ff"
            }
        })
    ),
    rowOutOfDate: style({
        backgroundColor: "#fff0f0",
        cursor: "pointer",
        transition: "background-color 0.25s ease",
        ":hover": {
            backgroundColor: "#0096ff"
        }
    }),

    alignRight: style({
        textAlign: "right"
    }),
    navbar: style({
        width: "100%",
        height: "60px",
        borderBottom: "1px solid #222",
        backgroundColor: "white",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }),
    myNav: style({
        width: "100%",
        height: "30px",
        borderBottom: "1px solid #000000",
        backgroundColor: "#efefef",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }),
    myNavLi: style({
        display: "inline-block",
        paddingRight: "20px",
        listStyle: "none",
        cursor: "pointer",
    }),
    logo: style({
        fontSize: "20px"
    }),
    statusDot: style({
        width: "16px",
        height: "16px",
        margin: "0 auto",
    }),
    centerStatusDot: style({
        margin: "0px auto"
    }),
    groupSummaries: style({
        alignSelf: "flex-start",
        width: "400px",
        marginRight: "40px"
    }),
    leftIndent: style({
        marginLeft: "20px"
    }),
    strike: style({
        textDecoration: "line-through"
    }),
    verticalContainer: style({
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        marginLeft: "200px",
        //clear:"both",
        width: "100%",
        marginTop: "40px",
        marginBottom: "40px",
        //height:"auto",
        //display:"block"
    }),
    tableContainer: style({
        width: "100%",
        display: "flex",
        justifyContent: "center"
    }),
    table: style({
        border: "1px solid #ccc",
        borderCollapse: "collapse"
    }),
    row: style({
        width: "100%",
        border: "1px solid #000000",
        display: "flex",
        flexDirection: "row",
        jsutifyContent: "center"
    }),
    device3rdColumn: style({
        width: "340",
        borderRight: "1px solid #000000",
        padding: "4px"
    }),
    rowOffline: style({
        backgroundColor: "#adadad",
        cursor: "pointer",
        transition: "background-color 0.25s ease",
        ":hover": {
            backgroundColor: "#ff5757"
        }
    }),
    cell: style({
        padding: "5pm",
        border: "1px solid #ccc"
    }),
    info: style({
        padding: "10px"
    }),
    paragraph: style({
        marginBottom: "10px"
    }),
    form: style({
        padding: "10px",
        display: "flex",
        flexDirection: "column"
    }),
    label: style({
        display: "flex",
        flexDirection: "right",
        // border: "1px solid #ff0000",
        fontFamily: "'Futura Light'",
        fontSize: "1.1rem",
        margin: "0px 0px 10px 0px",
        padding: "0px",
        // ":last-child": {
        //   margin: "0"
        // }
    }),
    input: style({
        padding: "5px"
    }),
    textarea: style({
        padding: "5px",
        width: "300px",
        height: "100px"
    }),
    footer: style({
        width: "100%",
        height: "60px",
        borderTop: "1px solid #222",
        color: "#222",
        fontSize: "10px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white"
    })
};

export default styles;