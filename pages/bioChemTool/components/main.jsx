import React from 'react';
import styles from './styles';

// Primary Components
import SceneController from './sceneController'; // Main component that is holding all the scene data


class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (<SceneController />);
  }
}

export default Main;