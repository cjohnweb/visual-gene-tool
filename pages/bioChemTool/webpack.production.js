const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

module.exports = {
  entry: [/*'babel-polyfill',*/ path.join(__dirname, 'bioChemTool.jsx')],
  output: {
    filename: './pages/bioChemTool/assets/bioChemTool-bundle.js'
  },
  module: {
    rules: [
      {
        test: [/\.jsx?$/, /\.js?$/],
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2017', 'es2015', 'react']
            }
          }
        ]
      }
      //      {
      //        test: /\.less$/,
      //        use: ExtractTextPlugin.extract({
      //          fallback: 'style-loader',
      //          use: "css-loader?sourceMap!less-loader?sourceMap",
      //        })
      //      },
      //      {
      //        test: /\.css$/,
      //        use: [ 'style-loader', 'css-loader' ]
      //      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
      output: {
        comments: false,
      },
    }),
  ],
  resolve: {
    extensions: [".js", ".jsx"]
  }
};

