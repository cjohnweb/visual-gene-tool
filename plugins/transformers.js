module.exports = {
/*     Linux System Functions       */
	diskuse : function (data) {
		//console.log(data)
		if(data){
			return data.split('\n')
			.filter( str => /rootfs/i.test(str) || /spiffs/i.test(str))
			.map( str => str.replace(/\s+/, ' ').split(' ').filter( s => s != ''))
			.reduce( (a,n,i,ar)=>{
				var b = {}
				b.filesystem = n[0];
				b.size = n[1];
				b.used = n[2];
				b.available = n[3];
				b.usepercent = n[4];
				b.mounted = n[5];
				return b;
				}, ({})
			)
		}else{
			return false
		}
	},
	
	cpuload: function(cpuload){
		if (cpuload){

			// Format the CPULoad String
			cpuload = cpuload.replace(/\s+/, '').split(',').map(str => str.replace('load average: ','').replace('\n','').trim())
			//cpuload[0].split(' up ')
			cpuload[0] = cpuload[0].split(' up ')
			//console.log("POST CPU LOG: "+cpuload)
			var a = {}
			if (cpuload.length <= 5){
				// [['15:08:40','6 min'],'1 user','0.33','0.35','0.18']
				a.time = cpuload[0][0]
				a.users = cpuload[1]
				a.running = cpuload[0][1]
				a.avgs = cpuload[2]+" "+cpuload[3]+" "+cpuload[4]
			}else{
				// [['15:02:09','2 days'],'22:30','1 user','0.50','0.49','0.38']
				a.time = cpuload[0][0]
				a.users = cpuload[2]
				a.running = cpuload[0][1]+" & "+cpuload[1]
				a.avgs = cpuload[3]+" "+cpuload[4]+" "+cpuload[5]
			}
			return a
		}else{
			return false
		}
	},
	networking: function(a){
		if(a){
			a = a.split(/\n\n/)
			.map(x => x.match(/^([A-z]*\d*)\s+Link\s+encap:([A-z0-9]*)\s+HWaddr\s+([A-z0-9:]*)\s+inet addr:([0-9.]+)\s+Bcast:([0-9.]+)\s+Mask:([0-9.]+)\s+/im))
			.filter(s => s != null)
			.map(a => a.slice(1,7))
			.map(
				(n)=>{
					var b = {}
					b.name = n[0];
					b.type = n[1];
					b.mac = n[2];
					b.ip = n[3];
					b.broadcast = n[4];
					b.netmask = n[5];
					return b;
				}
			)
		}else{
			a = false
		}
		return a
	},

	memory: function(str){
		if(str){
			var a = {}
			var b = {}
			a = str.split(/\s+/).filter( s => s != '' && !/mem/i.test(s) )
			b.total = a[0];
			b.used = a[1];
			b.free = a[2];
			b.shared = a[3];
			b.buffers = a[4];
			b.cached = a[5];		
			return b;
		}else{
			return false
		}
	},

/*     ESP / Micro Functions       */

	esp:{
		diskuse: function(str){
			return [str.split(' ')] // [["SPIFFS", "2949250B", "221382B", "2727868B", "0%", "\/"]]
			.reduce( (a,n,i,ar)=>{
				a.filesystem = n[0];
				a.size = n[1];
				a.used = n[2];
				a.available = n[3];
				a.usepercent = n[4];
				a.mounted = n[5];
				return a;
			}, ({}))
		},
		cpuload: function(cpuload){
			if (cpuload){
				cpuload = cpuload.split(/[,{1}up{1}]+/).map( str => str.replace(/\s+/, '')).filter( s => s != '' )
				// [ '00:00:00', '0 days', '8:24' ]
				var a = {}
				a.time = cpuload[0]
				a.users = 0
				a.running = cpuload[1]+" & "+cpuload[2]
				return a
			}else{
				return false
			}
		},
		memory: function(str){
			if(str){
				var a = {}
				var b = {}
				a = str.split(/\s+/).filter( s => s != '' && !/mem/i.test(s) )
				b.total = a[0];
				b.used = a[1];
				b.free = a[2];
				b.shared = a[3];
				b.buffers = a[4];
				b.cached = a[5];		
				return b;
			}else{
				return false
			}
		}
	}
};