var mysql = require('mysql');
var env = require('dotenv').config()

// ENVIRONMENT LOADS
var NODE_ENV = process.env.NODE_ENV || 'development';
var host = process.env.db_host || 'heartbeatiot.com';
var user = process.env.db_user || 'johnLaptop';
var password = process.env.db_password || '$strangerThings(555);';
var database = process.env.db_database || 'heartbeatiot';

if(NODE_ENV == 'development' && (!host || !user || !database)){

	console.log('\n\n******************\n\nMySQL credentials not configured. Please add MySQL credentials to .env file.\n\n******************\n\n');
	console.log("Host: ", host);
	console.log("User: ", user);
	console.log("Database: ", database);
	console.log('\n\n******************\n\n');
}else{
	console.log("Connecting to MySQL");
	var heartbeatDB = mysql.createPool({
		connectionLimit: 10,
		host     : host,
		user     : user,
		password : password,
		database : database
	});

	heartbeatDB.query('SELECT 1 + 1 AS solution', (error, results, fields) => {
		if (error) throw error;
		console.log('Database connection appears to be successful: ', results[0].solution ? 'true' : 'false');
	});

}

module.exports = heartbeatDB;
