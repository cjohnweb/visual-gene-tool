/**
 * Session Manager for HeartbeatIoT
 * @author John Minton <john.minton@jlmei.com> 
 * @version 0.0.0
 * 
 */

/**
 * class SessionManager
 * Manages the users session, which includes
 *  Creating and destroying sessions
 *  and stuff
 */

module.exports = class SessionManager {

	constructor(s) {
		// Pass that session baby in
		this.session = s;
		// If there is no user object, make one
		if (this.session.user == null) {
			this.session.user = { id: 0 };
		}
		return this;
	}

	set authentication(a) {
		try {
			this.session.user = {}
			this.session.user.authenticated = a.authenticated
			this.session.user.id = a.id
			this.session.user.name = a.name
			this.session.user.email = a.email
			this.session.user.permissions = a.permissions
			return true;
		} catch (e) {
			console.log(e)
			return undefined;
		}
	}

	set destroyAuthentication(a) {
		try {
			delete this.session.user
			return true;
		} catch (e) {
			return undefined;
		}
	}

	get authentication() {
		try {
			return this.session.user
		}
		catch (e) {
			return undefined;
		}
	}

	get UserID() {
		try {
			return this.session.user.id
		}
		catch (e) {
			return undefined;
		}
	}

	get isAuthenticated() {
		if (this.session.user.id) {
			return this.session.user.id
		} else {
			return undefined;
		}
	}
	thing() {
		return this.session;
	}
}