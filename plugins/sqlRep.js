module.exports = {
	devices: {
		create: {},



		read: {
			allDevices: 'SELECT id AS deviceID,device_id AS deviceSerial,device_servertime AS timestamp FROM devices',
			/*
			// allDevicesDetailed : These data's are not required at this time, so i commented them out here.
			device_installed AS deviceInstalled,
			device_final AS deviceFinal,
			device_permissions AS device,
			device_software AS device, 
			*/
			allDevicesDetailed: 'SELECT id AS deviceID, device_id AS deviceSerial, device_type AS deviceType, device_platform AS devicePlatform, device_lip AS deviceLAN, device_wip AS deviceWAN, device_hbversion AS deviceHeartbeatVersion, device_timestamp AS deviceServerLastComm, device_unittime AS deviceUnitLastComm, device_servertime AS deviceServerTime, device_uptime AS deviceUptime, device_state AS deviceData, device_notes AS deviceCustomerName, device_cpu_temp AS deviceCPTemp, device_data AS deviceESPData, device_heartbeatclient AS deviceHeartbeatClient FROM devices',
			getIDFromSerial: 'SELECT id FROM devices WHERE device_id=? LIMIT 1',
			getDeviceData: 'SELECT * FROM devices WHERE device_id=? LIMIT 1',
			getLastHeartbeat: 'SELECT device_servertime FROM devices WHERE device_id=? LIMIT 1',
			getWIPFromSerial: "SELECT device_wip FROM devices WHERE device_id=?",
			getIDsFromWIP: "SELECT id,device_id,device_notes,device_servertime FROM devices WHERE device_wip=?",
			getGroupsFromID: "SELECT dg_groupid FROM devices_groups WHERE dg_deviceid=?",
			getGroupNamesFromIDs: "SELECT g_id,g_name FROM groups WHERE g_id IN (?)",
		},
		update: {
			deviceByID: 'UPDATE devices SET device_platform=?,device_installed=?,device_lip=?,device_wip=?,device_timestamp=?,device_unittime=?,device_servertime=?,device_uptime=?,device_hbversion=?,device_state=?,device_cpu_temp=?,device_software=? WHERE id=?',
			deviceTimestampByID: 'UPDATE devices SET device_timestamp=?,device_unittime=?,device_servertime=?,device_wip=? WHERE id=?'
		},
		delete: {}
	},

	users: {
		create: {},
		read: {
			login: 'SELECT * FROM users WHERE user_email=? && user_pass=? LIMIT 1',
			userDetails: 'SELECT user_id,user_user,user_email,user_perms,user_notifications,user_emailprefs FROM users WHERE user_id=? LIMIT 1',
			allUsersDetails: 'SELECT user_id,user_user,user_email,user_perms FROM users'
		},
		update: {
			deviceByID: 'UPDATE devices SET device_platform=?,device_installed=?,device_lip=?,device_wip=?,device_timestamp=?,device_unittime=?,device_servertime=?,device_uptime=?,device_hbversion=?,device_state=?,device_cpu_temp=?,device_software=? WHERE id=?',
			deviceTimestampByID: 'UPDATE devices SET device_timestamp=?,device_wip=? WHERE id=?'
		},
		delete: {}
	},

	newDevices: {
		create: {
			insertUnAuthdDevice: 'INSERT INTO new_devices (nd_deviceid,nd_timestamp) VALUES (?,?)'
		},
		read: {
			getIDFromSerial: 'SELECT nd_id FROM new_devices WHERE nd_deviceid=? LIMIT 1',
			getAll: 'SELECT * FROM new_devices'
		},
		update: {
			newDeviceTimestamp: 'UPDATE new_devices SET nd_timestamp=? WHERE nd_deviceid=?'
		},
		delete: {}
	},

	commands: {
		create: {},
		read: {
			getCommands: 'SELECT cmd_id, cmd_command FROM commands WHERE cmd_device=? && cmd_send=\'\'',
			getLastCommands: 'SELECT cmd_id,cmd_command,cmd_issued,cmd_send FROM commands WHERE cmd_device=? ORDER BY cmd_issued DESC LIMIT ?'
		},
		update: {
			markCommandsAsSent: 'UPDATE commands SET cmd_transaction=?, cmd_send=? WHERE cmd_id IN (?)' // Need to append (?,?,?,?) - one question mark for each value
		},
		delete: {}
	},
	messages: {
		create: {
			insertMessage: 'INSERT INTO messages (m_id,m_device,m_message,m_code,m_timestamp) VALUES (m_id,?,?,?,?)'
		},
		read: {
			getRecent: 'SELECT m_device,m_message,m_code,m_timestamp FROM messages WHERE m_device=? ORDER BY m_timestamp DESC LIMIT 6'
		},
		update: {},
		delete: {}
	},

	espDevices: {
		devices: {
			create: {},
			read: {},
			delete: {},
			update: {
				deviceByID: 'UPDATE devices SET device_platform=?,device_installed=?,device_lip=?,device_wip=?,device_timestamp=?,device_unittime=?,device_servertime=?,device_hbversion=?,device_state=?,device_cpu_temp=?,device_data=?,device_heartbeatclient=? WHERE device_id=?',
				deviceTimestampByID: 'UPDATE devices SET device_timestamp=?,device_wip=? WHERE id=?'
			}
		}
	},
	groups: {
		create: {},
		read: {
			getAllGroups: 'SELECT * FROM groups',
			getDevicesByGroupID: 'SELECT g.dg_deviceid AS deviceID,d.device_id AS deviceSerial,d.device_notes AS customerName,d.device_servertime AS timestamp FROM devices_groups g LEFT JOIN devices d ON g.dg_deviceid=d.id WHERE g.dg_groupid=?',
			getGroupName: 'select g_name as "name" from groups where g_id = ?'
		},
		update: {},
		delete: {}
	},
	users_groups: {
		create: {},
		read: {
			userGroups: 'SELECT ug.ug_groupid AS groupID, g.g_name as groupName FROM users_groups ug LEFT JOIN groups g ON ug.ug_groupid=g.g_id WHERE ug.ug_userid=?',
		},
		update: {},
		delete: {}
	},
}