var sessions = require("client-sessions");

module.exports = sessions({
	cookieName: 'HeartbeatAPIAuth', // cookie name dictates the key name added to the request object
	requestKey: 'session',
	secret: '$thisISthe3#D(jlmaf981t58timeACQ9001restrict);', // should be a large unguessable string
	duration: 48 * 60 * 60 * 1000, // how long the session will stay valid in ms
	activeDuration: 1000 * 60 * 60 * 24, // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
	cookie: {
		//path: '/ajax|/auth', // cookie will only be sent to requests under '/api'
		secure: true // when true, cookie will only be sent over SSL. use key 'secureProxy' instead if you handle SSL not in your node process
	}
})

