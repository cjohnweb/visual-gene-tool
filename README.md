## HeartbeatIoT (Incomplete)

### Table of Contents

1. [Installation](#installation)
1. [Running](#running)

## Installation

First, we need to install all of the dependencies:

```
npm run preinstall
npm install
```

Next, we need to compile our webpack bundles:

```
npm run watch
```

## Running

We can start the necessary servers using npm:

```
npm start
```

Or we can start each server independently.

We can start the backend server using npm:

```
npm run backend
```

We can start the frontend server using npm:

```
npm run frontend
```

Now , we can view the program in the browser at [http://localhost:7007](http://localhost:7007).
