module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var transformers = req.app.locals.transformers;
	var espTransformers = transformers.esp;

	req.app.locals.heartbeatDB.getConnection((error,connection) =>{
		// Get some initial variables set
		var routePrefix = req.originalUrl
		var WANip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
		WANip = WANip.replace(/^.*:/, '')
		var data = req.body
		var deviceID
		var deviceSerial = data.device
		var device_unittime = data.timestamp
		var nowTimestamp = req.app.locals.epoch()
		var transactionID = data.transaction
		//if(deviceSerial){console.log("Incoming Report from "+deviceSerial)}

		//if(deviceSerial.length < 1) return res.sendStatus(400).send("[{'error':'Insufficient Data Received.'}]") // If no data comes in

		// Check if the device already exists in the database
		return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.read.getIDFromSerial, deviceSerial, cb)}, {multiArgs: true})
		.then(function(results){ 				// Parse Results from DB
			var rows = results[0]  			// Pull the Row from the Results
			if(rows.length > 0){
				var _deviceID = rows[0].id 	// Otherwise, returns the deviceID if it does exist.
				deviceID = _deviceID 			// Set this here so I can use it below when pulling Commands
				return _deviceID				// return the device ID
			}else{ 								// else...insert into temporary table, then return false again
				var args = [deviceSerial]
				// Select if device name exists in new_devices table
				return Promise.fromCallback(function(cb){ return connection.query(SQLRep.newDevices.read.getIDFromSerial, args, cb)}, {multiArgs: true})
				.then( function(results){
					var rows = results[0]  			// Pull the Row from the Results
					
					//console.log(rows)
					if(rows.length > 0){
						consoleColumn([routePrefix,deviceSerial,"Device not authorized: UPDATE new_devices"])
						//console.log(routePrefix+"\t"+deviceSerial+": \tDevice not authorized: UPDATE new_devices")
						var args = [nowTimestamp,deviceSerial]
						return Promise.fromCallback(function(cb){ return connection.query(SQLRep.newDevices.update.newDeviceTimestamp, args, cb)}, {multiArgs: true})
						.then( function(results){
							var updateResults = results[0]
						})

					}else{
						// If device exists in new_devices, update otherwise insert
						consoleColumn([routePrefix,deviceSerial,"Device not authorized: INSERT new_devices"])
						//console.log(routePrefix+"\t"+deviceSerial+": \tDevice not authorized: INSERT new_devices")
						var args = [deviceSerial,nowTimestamp]
						return Promise.fromCallback(function(cb){ return connection.query(SQLRep.newDevices.create.insertUnAuthdDevice, args, cb)}, {multiArgs: true})
						.then( function(results){
							var updateResults = results[0]
						})
					}
					throw 'Device Not Authorized'
				})
			}
		})

		.then((_deviceID)=>{

			// Only send commands over HTTPS
			if(req.secure){

				return Promise.fromCallback(function(cb){return connection.query(SQLRep.commands.read.getCommands, deviceSerial, cb)}, {multiArgs: true})
				// Now we gotta create a JSON communication object and send it back to the client
				.then( function(results){
					// transactionID comes from the original data sent
					//var transactionID = crypto.createHash('md5').update(Date()+Math.random()).digest("hex")
					var getCommands = results[0]
					if(getCommands.length){
						commandCount = getCommands.length
						// Format commands and send to client
						var commands = getCommands.map(function(row){				// Map a transformer over it, returns an array ( ['item','item2','etc'] )
							return row.cmd_command									// Save this itteration into the commands array...
						})
						commands = Object.assign({}, commands) 					// Converts the Array to an Object, with indexing keys: {'0':'item1', '1':'item2','2':'etc'}
						// Returns an array ( ['item','item2','etc'] )
						var commandIDs = getCommands.map(function(row){				// Map a transformer over it
							return row.cmd_id										// Save this itteration into the commands array...
						})
						var args = [transactionID,nowTimestamp,commandIDs]
						return Promise.fromCallback(function(cb){return connection.query(SQLRep.commands.update.markCommandsAsSent, args, cb)}, {multiArgs: true})
						.then(() => {
							return res.json({
							"timestamp": nowTimestamp,
							"device": "heartbeat.measurz.net",
							"transaction": transactionID,
							"requesttype": "response",
							"request": commands,
							"status": {
								"code": 1,
								"message": "Reporting successful."
								}
							})
						})

					}else{

						return res.json({
						"timestamp": nowTimestamp,
						"device": "heartbeat.measurz.net",
						"transaction": transactionID,
						"requesttype": "response",
						"request": null,
						"status": {
							"code": 1,
							"message": "Reporting successful."
							}
						})
					}
				})
			
			/*
			If over HTTP, check if commands are waiting in Queue only - do not send the commands
			Actually, the Linez/Loadz may not be reporting over HTTPS at all at this time. We first need to 
			ensure that the Linez Loads has support for this, then modify the code below to only check for 
			commands instead of returning the commands them selves.

			If commands are present in the database, return as such:

			[...]
			"requesttype": "response",
			"request": "CommandsQueued",
			"status": {
			[...]

			*/
			}else{

				return Promise.fromCallback(function(cb){return connection.query(SQLRep.commands.read.getCommands, deviceSerial, cb)}, {multiArgs: true})
				// Now we gotta create a JSON communication object and send it back to the client
				.then( function(results){
					// transactionID comes from the original data sent
					//var transactionID = crypto.createHash('md5').update(Date()+Math.random()).digest("hex")
					var getCommands = results[0]
					if(getCommands.length){
						commandCount = getCommands.length
						// Format commands and send to client
						var commands = getCommands.map(function(row){				// Map a transformer over it, returns an array ( ['item','item2','etc'] )
							return row.cmd_command									// Save this itteration into the commands array...
						})
						commands = Object.assign({}, commands) 					// Converts the Array to an Object, with indexing keys: {'0':'item1', '1':'item2','2':'etc'}
						// Returns an array ( ['item','item2','etc'] )
						var commandIDs = getCommands.map(function(row){				// Map a transformer over it
							return row.cmd_id										// Save this itteration into the commands array...
						})
						var args = [transactionID,nowTimestamp,commandIDs]
						return Promise.fromCallback(function(cb){return connection.query(SQLRep.commands.update.markCommandsAsSent, args, cb)}, {multiArgs: true})
						.then(() => {
							return res.json({
							"timestamp": nowTimestamp,
							"device": "heartbeat.measurz.net",
							"transaction": transactionID,
							"requesttype": "response",
							"request": commands,
							"status": {
								"code": 1,
								"message": "Reporting successful."
								}
							})
						})

					}else{

						return res.json({
						"timestamp": nowTimestamp,
						"device": "heartbeat.measurz.net",
						"transaction": transactionID,
						"requesttype": "response",
						"request": null,
						"status": {
							"code": 1,
							"message": "Reporting successful."
							}
						})
					}
				})


			}


		})
		.then(function(){
			// Process Messages

			var message = data.status.message
			var code = data.status.code
			
			// Code 01 is standard reporting, contains no messages to report.
			// Code 02 is for messages, file transfers, etc.

			if(code != "01"){
				var args = [deviceID,message,code,nowTimestamp]
				return Promise.fromCallback(function(cb){return connection.query(SQLRep.messages.create.insertMessage, args, cb)}, {multiArgs: true})
				.then((results) => {
					// console.log("Saved Messages: ")
					// console.log(results[0])
					// console.log(message)
				})
			}
		})
		.then(function(){
			// Save the device data, reports save full data, heartbeat request types save only timestamp and maybe WAN IP
			var requesttype = data.requesttype
			var device_wip = WANip // Need to get the Users WAN IP and set it here.
			
			if(requesttype == "report"){

				var device_platform = data.request.device_platform
				var device_installed = 'true'
				var device_lip = data.request.lip
				//var device_unittime = data.timestamp // Set above, from device data
				var device_timestamp = nowTimestamp
				var device_servertime = nowTimestamp
				var device_hbversion = data.request.HeartbeatVersion
				// State, set below and stringified
				var device_cpu_temp = "n/a"
				// device_data field, also set below
				var HeartbeatClient = data.request.HeartbeatClient				
				var device_memory = data.request.memory
				var diskDetails = espTransformers.diskuse(data.request.diskuse)
				var CPULoadDetails = espTransformers.cpuload(data.request.cpuload)
				
				var device_state = JSON.stringify(
					{
						"wip":device_wip,
						"lip":device_lip,
						"version":device_hbversion,
						"cpu_temp":"null",
						"diskuse":diskDetails,
						"networking":{},
						"cpuload":CPULoadDetails,
						"memory":{
							"total":device_memory,
							"used":null,
							"free":null,
							"shared":null,
							"buffers":null,
							"cached":null
						}
					}
				)

				var device_data = JSON.stringify(data.request.LinezData) // Convert to JSON

				var args = [device_platform,device_installed,device_lip,device_wip,device_timestamp,device_unittime,device_servertime,device_hbversion,device_state,device_cpu_temp,device_data,HeartbeatClient,deviceID]
				return Promise.fromCallback(function(cb){return connection.query(SQLRep.espDevices.devices.update.deviceByID, args, cb)}, {multiArgs: true})
				.then(function(results){
					consoleColumn([routePrefix,deviceSerial,"Report Saved"])
					//console.log(routePrefix+"\t"+deviceSerial+": \tReport Saved")
				})
			}else if(requesttype == "heartbeat"){
				var args = [device_timestamp,device_wip,deviceID]
				return Promise.fromCallback(function(cb){return connection.query(SQLRep.espDevices.devices.update.deviceTimestampByID, args, cb)}, {multiArgs: true})
				.then(function(results){
					consoleColumn([routePrefix,deviceSerial,"Heartbeat Saved"])
					//console.log(routePrefix+"\t"+deviceSerial+": \tHeartbeat Saved")
				})
			}else{
				consoleColumn([routePrefix,deviceSerial,"Request Type "+requesttype+" NOT recognized"])
				//console.log(routePrefix+"\t"+deviceSerial+": \tRequest Type "+requesttype+" NOT recognized")
			}
		})
		.catch( e => {
			if(e == 'Device Not Authorized'){
				return res.status(401).send(e)
			}else if(e != 'No rows returned'){
				console.error(Date(), ':reportingRouter:', e)
				return res.sendStatus(500)
			}else{
				res.sendStatus(404)
			}
		})
		
		.finally( () => connection.release())
	})
}
