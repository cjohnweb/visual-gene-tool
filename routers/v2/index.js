/**
 * Legacy / Version 1 Router for Version 1  Data Reporting
 * @author John Minton <john.minton@jlmei.com> 
 * @version 0.0.0
 */
var Router = require('express').Router;
var bodyParser = require('body-parser');			// Body parser used for parsing POST data
//var crypto = require('crypto');

module.exports = function(secure){
	// make the router
	var router = Router();
	// HTTPS Routes
	if(secure){

		// Post Request
		router.post('/', require(__dirname+'/handlers/reporting'));

		// Post Request for Micro
		router.post('/micro', require(__dirname+'/handlers/reporting_micro'))

		// Authentication Route
		//router.post('/login', require(__dirname+'/handlers/login/getSession'))
		//router.get('/login', require(__dirname+'/handlers/login/fetchSession'))
	
		
	
	// HTTP Routes
	}else{

		// Post Request
		//router.post('/', require(__dirname+'/handlers/reporting')(Promise, transformers, consoleColumn) );
		router.use('/', (req, res) => { return res.sendStatus(404) } );
		
		// Post Request for Micro
		router.post('/micro', require(__dirname+'/handlers/reporting_micro'))

	}

	return router;

}