module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var transformers = req.app.locals.transformers;
	var espTransformers = transformers.esp;
	var uuidv1 = require('uuid/v1');	


	req.app.locals.heartbeatDB.getConnection((error,connection) =>{
		// Get some initial variables set
		var routePrefix = req.originalUrl
		var WANip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress).replace(/^.*:/, '')
		var data = req.body
		var deviceID
		var deviceSerial = data.device
		var device_unittime = data.timestamp
		var nowTimestamp = req.app.locals.epoch()
		var transactionID = data.transaction
		var UUID = data.UUID // Incorporating a UUID to all devices...need to impliment to the client side still...


		//if(deviceSerial){console.log("Incoming Report from "+deviceSerial);}
		//if(deviceSerial.length < 1) return res.sendStatus(400).send("[{'error':'Insufficient Data Received.'}]"); // If no data comes in

		// Check if the device already exists in the database
		return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.read.getIDFromSerial, deviceSerial, cb)}, {multiArgs: true})
		.then(function(results){ 				// Parse Results from DB
			var rows = results[0]  			// Pull the Row from the Results
			if(rows.length > 0){
				deviceID = rows[0].id 	// Otherwise, returns the deviceID if it does exist.
			}else{ 								// else...insert into temporary table, then return false again
				var args = [deviceSerial]
				// Select if device name exists in new_devices table
				return Promise.fromCallback(function(cb){ return connection.query(SQLRep.newDevices.read.getIDFromSerial, args, cb)}, {multiArgs: true})
				.then( function(results){
					var rows = results[0]  			// Pull the Row from the Results
					//console.log(rows)
					if(rows.length > 0){

						consoleColumn([routePrefix,deviceSerial,"Device not authorized: UPDATE new_devices"])
						//console.log(routePrefix+"\t"+deviceSerial+": \tDevice not authorized: UPDATE new_devices")

						var args = [nowTimestamp,deviceSerial]
						return Promise.fromCallback(function(cb){ return connection.query(SQLRep.newDevices.update.newDeviceTimestamp, args, cb)}, {multiArgs: true})
						.then( function(results){
							var updateResults = results[0]
							throw 'Device Not Authorized'
						})

					}else{
						// If device exists in new_devices, update otherwise insert
						consoleColumn([routePrefix,deviceSerial,"Device not authorized: INSERT new_devices"])
						//console.log(routePrefix+"\t"+deviceSerial+": \tDevice not authorized: INSERT new_devices")
						var args = [deviceSerial,nowTimestamp]
						return Promise.fromCallback(function(cb){ return connection.query(SQLRep.newDevices.create.insertUnAuthdDevice, args, cb)}, {multiArgs: true})
						.then( function(results){
							var updateResults = results[0]
							throw 'Device Not Authorized'
						})
					}
				})
			}
		})

		.then(()=>{
			if(!UUID){
				// If the client did not report a UUID, then generate a UUID and assign it to the device in the commands.
				// If such is the case, then also save the UUID in the database for the given device based upon deviceSerial for the time being...

				// Generate UUID
				UUID = uuidv1()
				consoleColumn([routePrefix,deviceSerial,UUID])
				console.log('I need to save this UUID to the device as a command, device should save the UUID')
				// Save this in the devices table in the database...
				//confirm that the remote unit has saved this...
				
			}

		})
		// Get commands
		.then(function(){
			var args = [deviceID]
			return Promise.fromCallback(function(cb){return connection.query(SQLRep.commands.read.getCommands, args, cb)}, {multiArgs: true})
		})
		// Now we gotta create a JSON communication object and send it back to the client
		.then( function(results){
			// transactionID comes from the original data sent
			//var transactionID = crypto.createHash('md5').update(Date()+Math.random()).digest("hex")
			var getCommands = results[0]
			if(getCommands.length){
				commandCount = getCommands.length
				// Format commands and send to client
				var commands = getCommands.map(function(row){				// Map a transformer over it, returns an array ( ['item','item2','etc'] )
					return row.cmd_command									// Save this itteration into the commands array...
				})
				commands = Object.assign({}, commands) 					// Converts the Array to an Object, with indexing keys: {'0':'item1', '1':'item2','2':'etc'}
				// Returns an array ( ['item','item2','etc'] )
				var commandIDs = getCommands.map(function(row){				// Map a transformer over it
					return row.cmd_id										// Save this itteration into the commands array...
				})
				var args = [transactionID,nowTimestamp,commandIDs]
				return Promise.fromCallback(function(cb){return connection.query(SQLRep.commands.update.markCommandsAsSent, args, cb)}, {multiArgs: true})
				.then(() => {
					return res.json({
					"timestamp": nowTimestamp,
					"device": "heartbeat.measurz.net",
					"transaction": transactionID,
					"requesttype": "response",
					"request": commands,
					"status": {
						"code": 1,
						"message": "Reporting successful."
						}
					})
				})

			}else{

				return res.json({
				"timestamp": nowTimestamp,
				"device": "heartbeat.measurz.net",
				"transaction": transactionID,
				"requesttype": "response",
				"request": null,
				"status": {
					"code": 1,
					"message": "Reporting successful."
					}
				})
			}
		})
		.then(function(){

			// Process Messages
			var message = data.status.message
			var code = data.status.code
			
			// Code 01 is standard reporting, contains no messages to report.
			// Code 02 is for file transfers

			if(code != "01"){
				var args = [deviceID,message,code,nowTimestamp]
				return Promise.fromCallback(function(cb){return connection.query(SQLRep.messages.create.insertMessage, args, cb)}, {multiArgs: true})
				.then((results) => {
					// console.log("Saved Messages: ")
					// console.log(results[0])
					// console.log(message)
				})
			}
		})
		.then(function(){
			// Save the device data, reports save full data, heartbeat request types save only timestamp and maybe WAN IP
			var requesttype = data.requesttype
			var device_wip = WANip // Need to get the Users WAN IP and set it here.
			if(requesttype == "report"){
				var device_platform = data.request.device_platform
				var device_installed = "true"
				var device_lip = data.request.lip
				var device_hbversion = data.request.HeartbeatVersion
				//var device_unittime = data.timestamp // Set above, from device data
				var device_timestamp = nowTimestamp
				var device_servertime = nowTimestamp
				var device_uptime = data.request.uptime
				var device_cpu_temp = data.request.cpu_temp
				var device_data = ""
				var device_software = ""
				var device_state = {}
				//device_state.device_id = deviceSerial
				device_state.diskuse = transformers.diskuse(data.request.diskuse) // Disk Use, Thanks Robert
				device_state.cpuload = transformers.cpuload(data.request.cpuload) // CPU Load
				device_state.networking = transformers.networking(data.request.networking) // Networking
				device_state.memory = transformers.memory(data.request.memory) // Memory
				device_state = JSON.stringify(device_state) // Convert to JSON
				var args = [device_platform,device_installed,device_lip,device_wip,device_timestamp,device_unittime,device_servertime,device_uptime,device_hbversion,device_state,device_cpu_temp,device_software,deviceID]
				return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.update.deviceByID, args, cb)}, {multiArgs: true})
				.then(function(results){
					consoleColumn([routePrefix,deviceSerial,"Report Saved"])
					//console.log(routePrefix+"\t"+deviceSerial+": \tReport Saved")
				})
			}else if(requesttype == "heartbeat"){
				var args = [device_timestamp,device_wip,deviceID]
				return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.update.deviceTimestampByID, args, cb)}, {multiArgs: true})
				.then(function(results){
					consoleColumn([routePrefix,deviceSerial,"Heartbeat Saved"])
					//console.log(routePrefix+"\t"+deviceSerial+": \tHeartbeat Saved")
				})
			}else{
				consoleColumn([routePrefix,deviceSerial,"Request Type "+requesttype+" NOT recognized"])
				//console.log(routePrefix+"\t"+deviceSerial+": \tRequest Type "+requesttype+" NOT recognized")
			}

		})
		.catch( e => {
			if(e == 'Device Not Authorized'){
				return res.status(401).send(e)
			}else if(e != 'No rows returned'){
				console.error(Date(), ':reportingRouter:', e)
				return res.sendStatus(500)
			}else{
				res.sendStatus(404)
			}
		})
		
		.finally( () => connection.release())
	})
}