 module.exports = function(req,res){
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	var data = {warning: "This route is HTTPS only."}
	consoleColumn([routePrefix,null,"HTTPS Only"])
	if(process.env.PRETTY_PRINT_RESPONSE){
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(data,false,4));
	}else{
		res.json(data);
	}
}
