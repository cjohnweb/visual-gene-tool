 module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var deviceSerial = req.params.serial
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	//var WANip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	//WANip = WANip.replace(/^.*:/, '')

	req.app.locals.heartbeatDB.getConnection((error,connection) => {

		var deviceID
		consoleColumn([routePrefix,deviceSerial,"Returning Device State"])

		return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.read.getDeviceData, deviceSerial, cb)}, {multiArgs: true})
		.then(function(results){ 				// Parse Results from DB
			var rows = results[0]				// Pull the Row from the Results
			if(rows.length > 0){
				return rows.map( function(v,i,a){
					try{
						data = JSON.parse(v.device_state)
						if(data.hasOwnProperty('deviceid')){
							delete data.deviceid;
						}
						data.device_id = v.id
						data.device_serial = v.device_id
						data.device_type = v.device_type
						data.customerName = v.device_notes
						data.reverseSSHPort = 30000+v.id 
						return data
					}catch(e){
						throw new Error ('Invalid Device State')
					}
				})
			}else{ 								// else...insert into temporary table, then return false again
				throw new Error ('No Rows Returned')
			}

			

		})
		.then(data => {
			data = data[0]

			// Set Commands based upon Device Type
			// Gridz
			if(data.device_type == 1){var deviceCommands = ["Start gridz.service","Stop gridz.service"]}
			// Powrz
			if(data.device_type == 2){var deviceCommands = ["PowrzMigration","PowrzMigrationRecover","UpdatePowrz","StartWindService","StopWindService"]}
			// Misc
			if(data.device_type == 3){var deviceCommands = []}
			// Server
			if(data.device_type == 4){var deviceCommands = []}
			// BMS-Master
			if(data.device_type == 5){var deviceCommands = []}
			// BMS-Slave
			if(data.device_type == 6){var deviceCommands = []}
			// Energizr-200
			if(data.device_type == 7){var deviceCommands = []}
			//Energizr-100 v2
			if(data.device_type == 8){var deviceCommands = ["fix_udhcpd","check_udhcpd"]}
			// Linez
			if(data.device_type == 9){var deviceCommands = ["ping 8.8.8.8","resolveHostname","scanMDNS","firmwareUpdate"]}
			// Loadz
			if(data.device_type == 10){var deviceCommands = ["ping 8.8.8.8","resolveHostname","scanMDNS","firmwareUpdate"]}
			// Phazr
			if(data.device_type == 11){var deviceCommands = []}
			// Phazr Hub
			if(data.device_type == 12){var deviceCommands = []}
			

			data.availableCommands = {
				"communications":["ReverseSSH "+data.reverseSSHPort,"Disconnect ReverseSSH","Generate & Sync New RSA Key"],
				"updates":["Update Heartbeat"],
				"debugging":["Services Status","SyncNTP","RestartNetworking","RestartSSH","WipeKnownHosts","Reboot","Indication","Indication10"],
				"networkMeasurements":["TraceRoute","SpeedTest"],
				"networkManagement":["Disable IPv6"],
				"systemManagement":["apt-get update","apt-get upgrade","apt-get-dist-upgrade","Install MTR","Install Expect"],
				"deviceSpecific":deviceCommands,
				"requestFile":["SendFile "]
				}
				
			return data
		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})
		.catch( e => {
			if(e.message == 'Invalid Device'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Invalid Device State'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		.finally( () => connection.release())
	})
}
