 module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var groupID = req.params.groupID
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.groupID ? ':groupID' : v).join('/')
	//var WANip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	//WANip = WANip.replace(/^.*:/, '')

	req.app.locals.heartbeatDB.getConnection((error,connection) => {

		consoleColumn([routePrefix,groupID,"Returning Group Devices"])

		return Promise.all([
			Promise.fromCallback(function(cb){return connection.query(SQLRep.groups.read.getDevicesByGroupID, groupID, cb)}),
			Promise.fromCallback(function(cb){return connection.query(SQLRep.groups.read.getGroupName, groupID, cb)})
		])
		.spread(function(devices, name){ 				// Parse Results from DB
			devices = devices//[0];  			// Pull the Row from the Results
			name = name//[0];
			if(!devices.length || !name.length){
				throw new Error ('Invalid Group')
			}
			//"groupName": groupName, 
			return { groupID, groupName: name[0].name, devices}
		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})		
		.catch( e => {
			if(e.message == 'Invalid Group'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		
		.finally( () => connection.release())
	})
}
