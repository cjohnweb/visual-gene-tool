 module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	var s = res.locals.session // Bring our sessions in as 's'

	if(req.params.userID){
		var userID = req.params.userID
	}else{
		var userID = s.UserID
	}

	req.app.locals.heartbeatDB.getConnection((error,connection) => {

		consoleColumn([routePrefix,userID,"Groups for User"])

		return Promise.fromCallback(function(cb){return connection.query(SQLRep.users_groups.read.userGroups, [userID], cb)}, {multiArgs: true})
		.then(function(results){ 
			var rows = results[0]
			if(rows.length > 0){
				return rows
			}else{
				throw new Error ('No Rows Returned')
			}
		})
		.then((data) => {
			console.log(data)
			return data

		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})
		
		.catch( e => {
			if(e.message == 'Invalid Device'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Invalid Device State'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		
		.finally( () => connection.release())
	})
}
