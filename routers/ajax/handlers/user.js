module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var deviceSerial = req.params.serial
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	//var userName = parseFloat(req.query.userName) // parseFloat ensures this is a number, for use with isNaN()
		//WANip = WANip.replace(/^.*:/, '')
	
	var userID = res.locals.session.UserID

	// ?userID=x
	if(req.query.userID){
		userID = parseFloat(req.query.userID)
	}

	req.app.locals.heartbeatDB.getConnection((error,connection) => {

		consoleColumn([routePrefix,userID,"user"])

		return Promise.fromCallback(function(cb){return connection.query(SQLRep.users.read.userDetails, [userID], cb)}, {multiArgs: true})
		.then(function(results){
			var rows = results[0]
			if(rows.length > 0){
				return rows[0]
			}else{
				throw new Error ('No Rows Returned')
			}
		})
		
		.then(function(data){
			data.user_perms = JSON.parse(data.user_perms)
			data.user_emailprefs = JSON.parse(data.user_emailprefs)
			data.user_notifications = JSON.parse(data.user_notifications)
			return data
		})
		.then(function(data){
			newData = {}
			newData.id = data.user_id
			newData.name = data.user_user
			newData.email = data.user_email
			newData.permissions = data.user_perms
			newData.emailPrefs = data.user_emailprefs
			newData.notifications = data.user_notifications
			return newData
		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})		
		.catch( e => {
			if(e.message == 'Invalid Device'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Invalid Device State'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		.finally( () => connection.release())
	})
}
