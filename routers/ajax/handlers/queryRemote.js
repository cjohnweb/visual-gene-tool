var https = require('https');

module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var deviceSerial = req.params.serial
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	//var WANip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	//WANip = WANip.replace(/^.*:/, '')
	
	var deviceID
	consoleColumn([routePrefix,deviceSerial,"Query Remote"])
	
	// return Promise.all(
	// 	[
	// 		new Promise(
	// 			(resolve, reject)=>setTimeout(resolve,1000,'1')
	// 		),
	// 		new Promise(
	// 			(resolve, reject)=>setTimeout(resolve,100,'2')
	// 		),
	// 		new Promise(
	// 			(resolve, reject)=>setTimeout(resolve,1,'3')
	// 		)
	// 	]
	// )
 	// .spread(function(a,b,c){
	// 	// console log the results of the API call
	// 	// Return to pass the results to the user
	// 	return [a,b,c]
	// })

	return new Promise(
		function(resolve, reject){
			try{
				var callback = function(response) {
					var str = ''
					// another chunk of data has been recieved, so append it to `str`
					// the whole response has been recieved, so we just print it out here
					response.on('data',  (chunk)=>{ str += chunk   } )
					response.on('error', (e    )=>{ reject(e)      } )
					response.on('end',   (     )=>{ resolve( str ) } )
					//response.on('catch', reject);
				}
			
				https.request( { host: 'opensnp.org', path: '/phenotypes/json/1.json', port: '443', method: 'GET'}, callback).on('error',function(e){reject(e)}).end();

			}catch(e){
				return reject(e)
			}
		}
	)
	.then(function(data){
		if(process.env.PRETTY_PRINT_RESPONSE){
			res.setHeader('Content-Type', 'application/json');
			res.send(JSON.stringify(JSON.parse(data),false,4)); //
		}else{
			res.json(data);
		}
	})
	.catch( e => {
		if(e.message == 'Invalid Device'){
			return res.status(404).send(e.message)
		}else if(e.message == 'Invalid Device State'){
			return res.status(404).send(e.message)
		}else if(e.message == 'No Results'){
			return res.status(404).send(e.message)
		}else if(e.message == 'No Rows Returned'){
			return res.status(404).send(e.message)
		}else if(e.message){
			return res.status(404).send({"error":e.message})
		}else{
			return res.sendStatus(500);
		}
	})
}
