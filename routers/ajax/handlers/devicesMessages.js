 module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var deviceSerial = req.params.serial
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	//var WANip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	//WANip = WANip.replace(/^.*:/, '')

	req.app.locals.heartbeatDB.getConnection((error,connection) => {

		var deviceID
		consoleColumn([routePrefix,deviceSerial,"Returning Device Messages"])

		return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.read.getIDFromSerial, deviceSerial, cb)}, {multiArgs: true})
		.then(function(results){ 				// Parse Results from DB
			var rows = results[0]  			// Pull the Row from the Results
			if(rows.length > 0){
				deviceID = rows[0].id 	// Otherwise, returns the deviceID if it does exist.
				return true
			}else{
				throw new Error ('Invalid Device')
			}
		})
		.then(function(cb){
			return Promise.fromCallback(function(cb){return connection.query(SQLRep.messages.read.getRecent, deviceID, cb)}, {multiArgs: true})
		})
		.then(function(results){
			return results;
		})
		.then(function(results){ 				// Parse Results from DB
			var rows = results[0]				// Pull the Row from the Results
			
			if(rows.length > 0){
				return rows;
			}else{ 								// else...insert into temporary table, then return false again
				consoleColumn([routePrefix,deviceSerial,"Bad Device ID"])
				throw new Error ('No Rows Returned')
			}
		})
		
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})
		
		.catch( e => {
			if(e.message == 'Invalid Device'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Invalid Device State'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		
		.finally( () => connection.release())
	})
}
