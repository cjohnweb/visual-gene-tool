 module.exports = function(req,res){

	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')

	req.app.locals.heartbeatDB.getConnection((error,connection) => {

		var searchQuery = req.params.searchQuery

		consoleColumn([routePrefix,searchQuery,"Search"])


		// A starting point for future advanced search query 

			// // Build the first part of the Query
			// $sql = "SELECT devices.device_id,devices.device_serial,devices.device_customername,devices.device_servertime,devices.device_type,groups.g_name,device_types.dt_name FROM devices ";
			// $sql .= "LEFT JOIN devices_groups on devices_groups.dg_deviceid = devices.device_id ";
			// $sql .= "LEFT JOIN groups on groups.g_id = devices_groups.dg_groupid ";
			// $sql .= "LEFT JOIN device_types on device_types.dt_id = devices.device_type ";
			// $sql .= "LEFT JOIN device_data on device_data.device_data_serial = devices.device_serial ";


			// If a filter has been set, filter, otherwise SELECT * FORM devices
			// if(isset($_GET['filter']) && !empty($_GET['filter'])){

			// 	$filter = trim($_GET['filter']); // Get the filter Query

			// 	// Parse Filter into SQL Segments
			// 	$pcs = explode(",",$filter);
			// 	// Check out each piece of the filter query
			// 	foreach ($pcs as $k => $v){
			// 		// If online/offline/intermit we will filter out devices after the SQL query executes and we get back results, so lets keep track of it here.
			// 		if($v == "online" || $v == "offline" || $v == "intermit"){
			// 			$filter_status[] = strtolower($v);
			// 		}
			// 		// real escape each part for SQL injection protection
			// 		$pcs[$k] = $mysqli->real_escape_string(trim($v));

			// 	}


			// 	// We have a filter query present so we need the WHERE clause and the filter terms

			// 	$sql .= "WHERE ";
			// 	foreach ($pcs as $k => $v){
			// 		$sql .= "devices.device_serial LIKE '%$v%' || devices.device_customername LIKE '%$v%' || ";
			// 		$sql .= "device_types.dt_name LIKE '%$v%' || ";
			// 		$sql .= "groups.g_name LIKE '%$v%' || ";
			// 		$sql .= "(device_data.device_data_key = 'platform' && device_data.device_data_json LIKE '%$v%') || ";
			// 	}
			// 	$sql = substr($sql,0,-4);

			// }

			// // Weather we are filtering or not we want to order our results
			// $sql .= " ORDER BY devices.device_type ASC, devices.device_serial ASC";


		searchQuery = '%'+searchQuery+'%'

		var args = [searchQuery,searchQuery]
		return Promise.fromCallback(function(cb){return connection.query("SELECT device_id,device_notes,device_servertime FROM devices WHERE device_id LIKE ? || device_notes LIKE ?", args, cb)}, {multiArgs: true})
		.then(function(results){
			var rows = results[0]
			if(rows.length > 0){
				return rows
			}else{
				throw new Error ('No Rows Returned')
			}
		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})
		.catch( e => {
			if(e.message == 'Invalid Device'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Invalid Device State'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		.finally( () => connection.release())
	})
}
