function isBetween(num, min, max) {
	return num >= min && num <= max;
}

 module.exports = function(req,res){
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var deviceSerial = req.params.serial
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	//var WANip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	//WANip = WANip.replace(/^.*:/, '')
	var commandsLimit = parseFloat(req.query.limit) // parseFloat ensures this is a number, for use with isNaN()

	if(isNaN(commandsLimit) || !isBetween(commandsLimit,1,100)){
		commandsLimit = 6
	}

	req.app.locals.heartbeatDB.getConnection((error,connection) => {
		
		var deviceID
		consoleColumn([routePrefix,deviceSerial,"Returning Commands"])

		// Check if the device already exists in the database
		var args = [deviceSerial,commandsLimit]
		return Promise.fromCallback(function(cb){return connection.query(SQLRep.commands.read.getLastCommands, args, cb)}, {multiArgs: true})
		.then(function(results){
			var rows = results[0]
			if(rows.length > 0){
				return rows.map( row => {return row} )
			}else{
				throw new Error ('No Rows Returned')
			}
		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})
		
		.catch( e => {
			if(e.message == 'Invalid Device'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Invalid Device State'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		.finally( () => connection.release())
	})
}
