 module.exports = function(req,res){
	
	// console.log("Sessions: ")
	// console.log(req.session)
	// 
	// if(req.session.seenyou) {
	// 	console.log("true")
	// }else{
	// 	console.log("false")
	// }
	
	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	// var s = res.locals.session // Bring our sessions in as 's'
	
	req.app.locals.heartbeatDB.getConnection((error,connection) => {
	
		consoleColumn([routePrefix,null,"Returning All Devices"]);

		return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.read.allDevicesDetailed, [], cb)}, {multiArgs: true})

			.then(function(results){ 				// Parse Results from DB
			var rows = results[0]				// Pull the Row from the Results
			if(rows.length > 0){
				rows.forEach( function(data,i,a){
					var temp;
					try{
						if(!data.deviceData){
							temp = "{}";
						}else{
							temp = JSON.parse(data.deviceData);
							data.deviceData = temp;
						}
					}catch(e){
						data.deviceData = {error: "Could not parse"};
					}
				})
				return rows;
			}else{ 								// else...insert into temporary table, then return false again
				throw new Error ('No Rows Returned')
			}
		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})
		.catch( e => {
			console.log("There was a catch...");
			if(e.message == 'Invalid Device'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Invalid Device State'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		.finally( () => connection.release())
	})
}
