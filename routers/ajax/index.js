/**
 * AJAX Router for Serving Data to Interfaces
 * @author John Minton <john.minton@jlmei.com> 
 * @version 0.0.0
 */

var Router = require('express').Router
var bodyParser = require('body-parser')

module.exports = function (secure) {
	// make the router
	var router = Router();
	//router.get('/queryRemote', require(__dirname+'/handlers/queryRemote'))
	router.get('/devices', require(__dirname + '/handlers/devicesAll'))
	router.get('/devicesDetailed', require(__dirname + '/handlers/devicesAllDetailed'))

	// // HTTPS Routes
	// if(secure){

	// 	// CRUD

	// 	// Create Actions
	// 	/**
	// 	* Add new Device
	// 	*	Required 	-	DeviceID 		-	Single Value
	// 	* 	Required 	- 	device_type 	- 	Single value
	// 	* 	Optional 	- 	groups 			- 	Array
	// 	*
	// 	* Create New Group
	// 	*	Required 	-	Group Name
	// 	*
	// 	* Create New Device Type
	// 	*	Required 	-	Device Type Name / ID
	// 	* 
	// 	* Add Device to Group
	// 	*	Required 	-	DeviceID
	// 	* 	Required 	-	GroupID
	// 	*
	// 	* Add User to Group
	// 	*	Required 	-	userID
	// 	* 	Required 	-	groupID
	// 	*
	// 	* Add Command to be parse by a device
	// 	*	Required 	-	DeviceSerial
	// 	* 	Required 	-	Command
	// 	*
	// 	*/

	// 	// Read Actions

	// 	// Device Related
	// 	router.get('/search/:searchQuery', require(__dirname+'/handlers/searchQuery'))
	// 	router.get('/devices', require(__dirname+'/handlers/devicesAll'))
	// 	router.get('/devicesDetailed', require(__dirname+'/handlers/devicesAllDetailed'))

	// 	router.get('/devices/new', require(__dirname+'/handlers/devicesNew'))
	// 	router.get('/devices/:serial', require(__dirname+'/handlers/devicesSerial'))
	// 	router.get('/devices/:serial/lastHeartbeat', require(__dirname+'/handlers/devicesLastHeartbeat'))
	// 	router.get('/devices/:serial/messages', require(__dirname+'/handlers/devicesMessages'))
	// 	router.get('/devices/:serial/commands/', require(__dirname+'/handlers/devicesCommands'))
	// 	router.get('/devices/:serial/network', require(__dirname+'/handlers/devicesNetwork'))
	// 	router.get('/devices/:serial/groups', require(__dirname+'/handlers/devicesGroups'))
	// 	//router.get('/devices/:serial/', require(__dirname+'/handlers/'))
	// 	//router.get('/devices/:serial/', require(__dirname+'/handlers/devicesThing'))

	// 	// Group Related
	// 	router.get('/groups', require(__dirname+'/handlers/groups'))
	// 	router.get('/group/:groupID', require(__dirname+'/handlers/groupsGetDevicesByGroupID'))

	// 	// User Related Data
	// 	router.get('/user', require(__dirname+'/handlers/user'))
	// 	router.get('/users', require(__dirname+'/handlers/users'))
	// 	router.get('/user/groups/:userID', require(__dirname+'/handlers/userGroups'))
	// 	router.get('/user/groups', require(__dirname+'/handlers/userGroups'))


	// 	// Update Actions
	// 	/**
	// 	* Update Device Customer Name
	// 	*	UPDATE devices SET device_notes=? WHERE id=?
	// 	*		Required 	-	DeviceID
	// 	*		Required 	-	customerName
	// 	*
	// 	* Update Device Type
	// 	*	UPDATE devices SET device_type=? WHERE id=?
	// 	*	if(int(typeID)){run query}else{query for typeID by name first, then proceed as ususal. If no type by name, return bad type}
	// 	*	Required 	-	DeviceID
	// 	* 	Required 	-	(int) typeID || (string) typeName
	// 	*	
	// 	*	
	// 	*
	// 	*/


	// 	// Delete Actions
	// 	/**
	// 	* delete device
	// 	* =============
	// 	* 	Required 	-	deviceID
	// 	*	Required 	-	deviceSerial
	// 	*   (Check that these both match the same device before proceeding)
	// 	*
	// 	*		DELETE FROM devices_groups WHERE dg_deviceid=?
	// 	*		 	Required 	-	deviceID
	// 	*		DELETE FROM messages WHERE m_device=?
	// 	*		 	Required 	-	deviceID
	// 	*		DELETE FROM commands WHERE cmd_device=?
	// 	*		 	Required 	-	deviceSerial
	// 	*		DELETE FROM noc_notification WHERE t_deviceid=?
	// 	*		 	Required 	-	deviceSerial
	// 	*		DELETE FROM notification_table WHERE t_deviceid=?
	// 	*		 	Required 	-	deviceSerial
	// 	*
	// 	* delete group
	// 	* =============
	// 	*	Required 	-	groupID
	// 	*		DELETE FROM groups WHERE g_id=?
	// 	*		 	Required 	-	groupID
	// 	*		DELETE FROM devices_groups WHERE dg_groupid=?
	// 	*		 	Required 	-	groupID
	// 	*		DELETE FROM users_groups WHERE ug_groupid=?
	// 	*		 	Required 	-	groupID
	// 	*
	// 	* delete user from users_groups
	// 	* =============
	// 	*	DELETE FROM users_groups WHERE ug_userid=? && ug_groupid=?
	// 	*		Required 	-	groupID
	// 	*		Required 	-	userID
	// 	*
	// 	* delete device from devices_groups
	// 	* =============
	// 	* 	DELETE FROM devices_groups WHERE dg_deviceid=? && dg_groupid=?
	// 	* 		Required	-	groupID
	// 	* 		Required	-	deviceID
	// 	*
	// 	*/

	// }

	return router

}