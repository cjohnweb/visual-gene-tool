/**
 * Returns the Groups that the given device belongs to
 */
module.exports = function(req,res){

	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var deviceSerial = req.params.serial
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	//var WANip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	//WANip = WANip.replace(/^.*:/, '')

	req.app.locals.heartbeatDB.getConnection((error,connection) => {
		
		var deviceID
		consoleColumn([routePrefix,deviceSerial,"Returning Device Groups"])

		// Get Device ID
		return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.read.getIDFromSerial, deviceSerial, cb)}, {multiArgs: true})

		.then((results)=>{
			deviceID = results[0][0].id
			// Get Devices Groups: dg_id dg_deviceid dg_groupid
			return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.read.getGroupsFromID, deviceID, cb)}, {multiArgs: true})
		})
		.then((results)=>{
			results = results[0]
			var groupIDs = []
			results.map((x)=>{
				groupIDs.push(x.dg_groupid) //String(x.dg_groupid)
			})
			var args = [groupIDs]
			// Get Names of Groups
			return Promise.fromCallback(function(cb){return connection.query(SQLRep.devices.read.getGroupNamesFromIDs, args, cb)}, {multiArgs: true})
		})
		.then((results)=>{
			results = results[0]
			var groups = []
			results.map((a)=>{
				groups.push({'id':a.g_id,'name':a.g_name})
			})
			var ret = {'serial':deviceSerial,'id':deviceID,'groups':groups}
			return ret
		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})
		.catch( e => {
			if(e.message == 'Invalid Device'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Invalid Device State'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
	})
}
