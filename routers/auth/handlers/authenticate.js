 module.exports = function(req,res){

	var SQLRep = req.app.locals.SQLRep;
	var Promise = req.app.locals.Promise;
	var consoleColumn = req.app.locals.consoleColumn;
	var nowTimestamp = req.app.locals.epoch()
	var routePrefix = req.originalUrl.split('/').map( v => v == req.params.serial ? ':serial' : v).join('/')
	var s = res.locals.session // Bring our sessions in as 's'
	
	var data = req.body
	var email = data.email
	var password = data.password

	req.app.locals.heartbeatDB.getConnection((error,connection) => {

		consoleColumn([routePrefix,email,"Authentication"])

		return new Promise(
			function(resolve, reject){
				try{
					if(email && password){
						var authDetails = [email, password]
						resolve(authDetails)
					}else{
						e.message = "userID and password are both required"
						reject(e)
					}
				}catch(e){
					return reject(e)
				}
			}
		)
		.then((authDetails)=>{
			return Promise.fromCallback(function(cb){return connection.query(SQLRep.users.read.login, authDetails, cb)}, {multiArgs: true})
		})
		.then(function(results){ 				// Parse Results from DB
			var rows = results[0]				// Pull the Row from the Results
			if(rows.length > 0){
				return rows[0]
			}else{ 								// else...insert into temporary table, then return false again
				throw new Error ('No Rows Returned')
			}
		})
		.then((results)=>{
			// Establish session / cookie data
			// Set cookie / userID
			user = {}
			user.authenticated = true
			user.id = results.user_id
			user.name = results.user_user
			user.email = results.user_email
			user.permissions = JSON.parse(results.user_perms)
			
			// Set Authentication Object to Session
			s.authentication = user;
			return {"message":"Authenticaion Cookie has been set. You are now logged in."}	
		})
		.then(function(data){
			if(process.env.PRETTY_PRINT_RESPONSE){
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify(data,false,4));
			}else{
				res.json(data);
			}
		})
		.catch( e => {

			if(e.message == 'userID and password are both required'){
				return res.status(404).send(e.message)
			}else if(e.message == 'Could Not Authenticate'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Results'){
				return res.status(404).send(e.message)
			}else if(e.message == 'No Rows Returned'){
				return res.status(404).send(e.message)
			}else if(e.message){
				return res.status(404).send(e.message)
			}else{
				return res.sendStatus(500);
			}
		})
		.finally( () => connection.release())
	})
}
