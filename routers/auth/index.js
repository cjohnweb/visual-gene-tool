/**
 * AJAX Router for Serving Data to Interfaces
 * @author John Minton <john.minton@jlmei.com> 
 * @version 0.0.0
 */

var Router = require('express').Router
var bodyParser = require('body-parser')

//since it can only ever be secure we can pass the same router to everyone
var router = Router();

// To set authentication cookie
router.post('/user/authenticate', require(__dirname+'/handlers/authenticate'))

// To destroy Authentication cookie and all cookie based user data
router.get('/user/deauthenticate',
	(r,s,n) => {
		// Destroys the entire session, not just authentication.  Figure out how to get the 
		r.session.destroy() // "set destroyAuthentication(a)" working in the SessionManagement class
		data = {"message":"Your authentication cookie has been destroyed."}
		if(process.env.PRETTY_PRINT_RESPONSE){
			s.setHeader('Content-Type', 'application/json');
			s.send(JSON.stringify(data,false,4));
		}else{
			s.json(data);
		}
	}
)

// To destroy Authentication cookie and all cookie based user data
router.get('/viewCredentials',
	(r,s,n) => {
		//var data = r.session.user
		var data = s.locals.session.authentication
		if(process.env.PRETTY_PRINT_RESPONSE){
			s.setHeader('Content-Type', 'application/json');
			s.send(JSON.stringify(data,false,4));
		}else{
			s.json(data);
		}
	}
)

// To set authentication Token in the Database
//router.post('/user/authenticateToken', require(__dirname+'/handlers/authenticateToken'))

// To destroy authentication Token in the Database
//router.post('/user/deauthenticateToken', require(__dirname+'/handlers/deauthenticateToken'))

// Safety middleware
function pass(r,s,next){
	next();
}


module.exports = function(secure){
	// make the router

	// HTTPS Routes
	if(secure){
		return router;
	}

	return pass

}